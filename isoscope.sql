-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 25, 2016 at 11:52 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `isoscope`
--

-- --------------------------------------------------------

--
-- Table structure for table `campaign`
--

CREATE TABLE IF NOT EXISTS `campaign` (
  `campaignId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaignName` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` datetime NOT NULL,
  `modifiedBy` int(11) NOT NULL,
  `modifiedDate` datetime NOT NULL,
  `deletedBy` int(11) NOT NULL,
  `deletedDate` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`campaignId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `campaign`
--

INSERT INTO `campaign` (`campaignId`, `campaignName`, `location`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`, `deletedBy`, `deletedDate`, `isDeleted`) VALUES
(1, 'Camp1', 'Camp1dasd', 1, '2016-09-23 18:47:16', 1, '2016-09-23 18:47:53', 0, '0000-00-00 00:00:00', 0),
(2, 'dsada', 'dasda', 1, '2016-09-23 18:48:39', 0, '0000-00-00 00:00:00', 1, '2016-09-23 18:48:44', 1),
(3, 'Camp2', 'Camp2', 1, '2016-09-23 20:36:34', 1, '2016-09-24 20:14:08', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `diseases`
--

CREATE TABLE IF NOT EXISTS `diseases` (
  `diseasesId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `diseasesName` varchar(255) NOT NULL,
  `diseasesShortCode` varchar(255) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` datetime NOT NULL,
  `modifiedBy` int(11) NOT NULL,
  `modifiedDate` datetime NOT NULL,
  `deletedBy` int(11) NOT NULL,
  `deletedDate` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL COMMENT '0->No,1->Yes',
  PRIMARY KEY (`diseasesId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `diseases`
--

INSERT INTO `diseases` (`diseasesId`, `diseasesName`, `diseasesShortCode`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`, `deletedBy`, `deletedDate`, `isDeleted`) VALUES
(1, 'Maleria', 'MMA', 1, '2016-09-24 20:40:24', 1, '2016-09-24 20:40:43', 0, '0000-00-00 00:00:00', 0),
(2, 'Dangue', 'dan', 1, '2016-09-24 20:40:54', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE IF NOT EXISTS `patient` (
  `patientId` int(11) NOT NULL,
  `campaignId` int(11) NOT NULL,
  `race` varchar(255) NOT NULL,
  `idNumber` varchar(255) NOT NULL,
  `disability` text NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `familyMemberName` varchar(255) NOT NULL,
  `familyMemeberPhoneNo` varchar(255) NOT NULL,
  `patientMedicalHistory` text NOT NULL,
  `allergies` text NOT NULL,
  `nearestHealthInstitution` text NOT NULL,
  `location` varchar(255) NOT NULL,
  PRIMARY KEY (`patientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`patientId`, `campaignId`, `race`, `idNumber`, `disability`, `nationality`, `familyMemberName`, `familyMemeberPhoneNo`, `patientMedicalHistory`, `allergies`, `nearestHealthInstitution`, `location`) VALUES
(2, 1, 'Patient1', '3224234', 'hearing impaired', 'india', 'sadsada', '4353535', 'sdfsdf', 'sdfsd', 'fsdfsd', 'fsdfs');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `roleId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `roleTitle` varchar(255) NOT NULL,
  `roleName` varchar(255) NOT NULL,
  `roleShortCode` varchar(255) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` datetime NOT NULL,
  `modifiedBy` int(11) NOT NULL,
  `modifiedDate` datetime NOT NULL,
  `deletedBy` int(11) NOT NULL,
  `deletedDate` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`roleId`, `roleTitle`, `roleName`, `roleShortCode`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`, `deletedBy`, `deletedDate`, `isDeleted`) VALUES
(1, 'Admin', 'admin', 'admin', 0, '2016-09-22 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(2, 'Patient', 'patient', 'patient', 0, '2016-09-22 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `roleId` int(11) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `gender` tinyint(4) NOT NULL COMMENT '1->male,2->female',
  `dateOfBirth` date NOT NULL,
  `phoneNo` varchar(255) NOT NULL,
  `profilePic` varchar(255) NOT NULL,
  `lastLoginDate` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` datetime NOT NULL,
  `modifiedBy` int(11) NOT NULL,
  `modifiedDate` datetime NOT NULL,
  `deletedBy` int(11) NOT NULL,
  `deletedDate` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL COMMENT '0->No,1->Yes',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `roleId`, `userName`, `password`, `email`, `firstName`, `lastName`, `gender`, `dateOfBirth`, `phoneNo`, `profilePic`, `lastLoginDate`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`, `deletedBy`, `deletedDate`, `isDeleted`) VALUES
(1, 1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'nidhi.barhate12590@gmail.com', 'admin', 'admin', 2, '1990-05-12', '46545646', '', '2016-09-25 08:18:05', 0, '1899-11-15 00:00:00', 1, '2016-09-24 20:21:19', 0, '0000-00-00 00:00:00', 0),
(2, 2, NULL, '', '', 'Patient1', 'Patient1', 2, '1982-12-02', '', '', '0000-00-00 00:00:00', 1, '2016-09-24 20:24:24', 1, '2016-09-25 08:20:35', 0, '0000-00-00 00:00:00', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
