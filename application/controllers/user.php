<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

    function __construct()
    {
        $this->accessRules = array(
            '*' => array('admin_login','ws_login','ws_register','ws_editProfile','ws_forgotPassword'),
            '@' => array('admin_logout','admin_view', 'admin_add','admin_get','admin_delete'),
            '$' => array(),
            '#' => array()
        );
        parent::__construct();

        $this->load->model('user_model','user_model');
        $this->load->model('role_model','role_model');
        $this->load->model('logoflogin_model','logoflogin_model');
    }

    public function admin_login(){
        $this->load->helper('cookie');
        $cookie=get_cookie('admin_login');

        if(!empty($cookie))
        {   
            $arrcookie=explode('&',$cookie);
            $this->min_check_login($arrcookie[0],$arrcookie[1],ROLE_ADMIN);
        }
        
        $arrData['userName'] = "";
        $arrData['password'] = "";

        if($this->input->post('eventSubmit') == 1){
            $data = $this->input->post();

            $userName = $data['userName'];
            $password = $data['password'];
            $this->form_validation->set_rules('userName', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == FALSE) {
                $arrData['userName'] = $userName;
                $arrData['password'] = $password;
            }
            else
            {
                $pwd = sha1($password); 
                
                if(isset($_REQUEST['remember_me']) && $_REQUEST['remember_me'] == 'on')
                {
                    $cookie = array(
                        'name'   => 'admin_login',
                        'value'  => $userName.'&'.$pwd,
                        'expire' => time()+86500*30,
                        );
                    $this->input->set_cookie($cookie);
                }
                $this->min_check_login($userName,$pwd,ROLE_ADMIN);
            }
        }

        $this->load->view('user/admin_login',$arrData);
    }

    /*
    * Check Login with inputs $userName,$password,$roleId
    */
    
   function min_check_login($userName,$password,$roleId,$isWS = '')
    {
        $this->load->model('user_model');
        $this->user_model->userOrEmail = $userName;
        $this->user_model->password = $password;
        
        
        //echo $userName." ".$password;die();
        $arrUsers = $this->user_model->check_admin_login();
        
        if($isWS == 1) 
        {
            if(!empty($arrUsers)){
                 $userdata = array(
                    'lastLoginDate'=> date(SQL_DATE_FORMAT)
                );
                $this->user_model->update($arrUsers[0]->userId, $userdata); 

                $logofdata['emailoruserName'] = $userName;
                $logofdata['password'] = $password;
                $logofdata['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $logofdata['message'] = 'Successfully Logged In.';
                $logofdata['createdDate'] = date(SQL_DATE_FORMAT);
                $this->logoflogin_model->insert($logofdata);
            }else{
                $logofdata['emailoruserName'] = $userName;
                $logofdata['password'] = $password;
                $logofdata['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $logofdata['message'] = 'Username or Password is incorrect.';
                $logofdata['createdDate'] = date(SQL_DATE_FORMAT);
                $this->logoflogin_model->insert($logofdata);
            }
            return $arrUsers;
        }
        if (!empty($arrUsers[0]))
        {
            $roleid = $arrUsers[0]->roleId;

            if($roleid == ROLE_ADMIN || $roleid == ROLE_PATHOLOGIST)
            {
                $this->session->set_userdata('admin_loggedin', 1);
                $this->session->set_userdata('admin_userid', $arrUsers[0]->userId);
                $this->session->set_userdata('admin_firstName', $arrUsers[0]->firstName);
                $this->session->set_userdata('admin_lastName', $arrUsers[0]->lastName);
                $this->session->set_userdata('admin_roleName', $arrUsers[0]->roleName);
                $this->session->set_userdata('admin_roleId', $arrUsers[0]->roleId);
                $userdata = array(
                    'lastLoginDate'=> date(SQL_DATE_FORMAT)
                );
                $this->user_model->update($arrUsers[0]->userId, $userdata); 

                $logofdata['emailoruserName'] = $userName;
                $logofdata['password'] = $password;
                $logofdata['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $logofdata['message'] = 'Successfully Logged In.';
                $logofdata['createdDate'] = date(SQL_DATE_FORMAT);
                $this->logoflogin_model->insert($logofdata);
                redirect(admin_url() . "user/view");
            }
            else
            {
                $logofdata['emailoruserName'] = $userName;
                $logofdata['password'] = $password;
                $logofdata['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $logofdata['message'] = 'You are not authorize user.';
                $logofdata['createdDate'] = date(SQL_DATE_FORMAT);
                $this->logoflogin_model->insert($logofdata);

                $this->session->set_flashdata('error_message', array('You are not authorize user.'));
                delete_cookie('admin_login');
                redirect(admin_url() . "user/login");
            }
        }
        else
        {
            $logofdata['emailoruserName'] = $userName;
            $logofdata['password'] = $password;
            $logofdata['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $logofdata['message'] = 'Username or Password is incorrect.';
            $logofdata['createdDate'] = date(SQL_DATE_FORMAT);
            $this->logoflogin_model->insert($logofdata);

            $this->session->set_flashdata('error_message', array('Username or Password is incorrect.'));
            delete_cookie('admin_login');
            redirect(admin_url() . "user/login");
        }
    }


    /*
    * Admin Logout
    */
    
    function admin_logout() {
        $this->session->sess_destroy(); 
        redirect(admin_url() . "user/login");
    }

    public function admin_view($roleId='')
    {
        $this->load->add_js = array('custom_js/user.js?v='.VERSION);
        $this->data['page_title'] = 'Manage User';
        $this->data['breadcrumbs'] = array(array("name"=>"User","link"=>"#"));
        $this->data['roleId'] = $roleId;
        $this->load->template('user/list',$this->data);  
    }

    public function admin_get(){
        $data = $this->input->post();
        $data['records'] = $this->user_model->get_user_records($data['roleId']);
        $arrResult = array();
        $this->load->view('user/get', $data); 
    }

    /*
    * Add User
    */

    function admin_add($id='') 
    { 
        $data['page_title'] = 'Add User';
        $data['breadcrumbs'] = array(array("name"=>"User","link"=>  site_url('admin/user/view')),array("name"=>"Add User","link"=>"#"));
        $data['userId'] = '';
        $data['userName'] = '';
        $data['roleId'] = '';
        $data['password'] = ''; 
        $data['email'] = ''; 
        $data['firstName'] = ''; 
        $data['lastName'] = ''; 
        $data['gender'] = ''; 
        $data['dateOfBirth'] = ''; 
        $data['phoneNo'] = '';   
        $data['profilePic'] = '';

        if($this->input->post('eventSubmit') == 1){
            $data['userId'] = $id;
            $data['roleId'] = $this->input->post('roleId');
            $data['userName'] = $this->input->post('userName');
            $data['password'] = sha1($this->input->post('password'));
            $data['email'] = $this->input->post('email');
            $data['firstName'] = $this->input->post('firstName');
            $data['lastName'] = $this->input->post('lastName');
            $data['gender'] = $this->input->post('gender');
            $data['dateOfBirth'] = date(SQL_DATE_FORMAT, strtotime($this->input->post('dateOfBirth')));
            $data['phoneNo'] = $this->input->post('phoneNo');

            if($id != ''){
                $sdata = (array)$this->user_model->get($id);
                foreach ($sdata as $key => $value)
                {
                    $sdata[$key] = $value;
                }
                if($data['userName'] != $sdata['userName']){
                    $this->form_validation->set_rules('userName', 'User Name', 'required|is_unique[users.userName]');
                }
            }else{
                $this->form_validation->set_rules('userName', 'User Name', 'required|is_unique[users.userName]');
            }

            $this->form_validation->set_rules('roleId', 'Role', 'required');
            if($id == '')
            {
                $this->form_validation->set_rules('password', 'Password', 'required');
            }
            $this->form_validation->set_rules('firstName', 'First Name', 'required');
            $this->form_validation->set_rules('lastName', 'Last Name', 'required');
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            if($id != '')
            {
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email[users.email.userId.'.$id.']|is_unique[users.email.userId.'.$id.']');
            }
            else
            {
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email[users.email]|is_unique[users.email]');
            }
            $this->form_validation->set_rules('dateOfBirth', 'Date Of Birth', 'required');
            $this->form_validation->set_rules('phoneNo', 'Phone No', 'integer');

            if ($this->form_validation->run() == FALSE) 
            {
                if($id != '')
                {
                    $data = (array)$this->user_model->get($id);
                    foreach ($data as $key => $value)
                    {
                        $data[$key] = $value;
                    }
                    $data['page_title'] = 'Edit User';
                    $data['breadcrumbs'] = array(array("name"=>"User","link"=>  site_url('admin/user/view')),array("name"=>"Edit User","link"=>"#"));
                    $data['role_list'] = $this->role_model->get_all();

                    $this->load->template('user/add',$data);   
                }
                else
                {
                    $data['page_title'] = 'Add User';
                    $data['breadcrumbs'] = array(array("name"=>"User","link"=>  site_url('admin/user/view')),array("name"=>"Add User","link"=>"#"));
                    $data['role_list'] = $this->role_model->get_all();
                    $this->load->template('user/add', $data);
                }
            }
            else
            {
                $userdata = array(
                    'userName'=> $data['userName'],
                    'roleId'=>$data['roleId'],
                    'email'=>$data['email'],
                    'firstName'=>$data['firstName'],
                    'lastName'=>$data['lastName'],
                    'gender'=>$data['gender'],
                    'dateOfBirth'=> $data['dateOfBirth'],
                    'phoneNo'=>$data['phoneNo']
                );

               /* if(!(empty($_FILES['profilePic']['name'])))
                {
                    $config['upload_path'] = UPLOAD_PATH_IMAGE;
                    $config['allowed_types'] = ALLOWED_TYPES_IMAGE;
                    
                    $this->load->library('upload', $config);
                    if($this->input->post('profilePicName') != '')
                    {

                        $old_image_file = $this->input->post('profilePicName');
                        $options['cdn_on'] = CDN_IMAGE_UPLOAD;
                        unlink(UPLOAD_PATH_IMAGE.$old_image_file);
                        unlink(UPLOAD_THUMB_IMAGE_PATH_USER.'thumb_'.$old_image_file);
                    }
                    $field_name = "profilePic";

                    $this->_file_name_override = 1;

                    $options['cdn_on']=CDN_IMAGE_UPLOAD;
                    $options['image_resize']=IMAGE_RESIZE;
                    $options['source_folder']=UPLOAD_PATH_IMAGE;
                    $options['thumb_folder']=UPLOAD_THUMB_IMAGE_PATH_USER;

                    if ( !$this->upload->do_upload($field_name, $options))
                    {
                         $error = array('error' => $this->upload->display_errors());
                         $this->session->set_flashdata('error_message', array($error['error']));
                         if($id != '' ) redirect(admin_url() . "user/add/".$id);
                         else  redirect(admin_url() . "user/add");
                         exit;
                    }
                    else
                    {
                        $data_upload = array('upload_data' => $this->upload->data());
                        $userdata['profilePic'] = $data_upload['upload_data']['file_name'];
                    }
                }else{
                    if($id != '') $userdata['profilePic'] =  $this->input->post('profilePic');
                    else $userdata['profilePic'] =  '';
                }*/
                 $userdata['profilePic'] =  '';
                if($id != ''){
                    $userdata['modifiedBy'] = $this->session->userdata('admin_userid');
                    $userdata['modifiedDate'] = date(SQL_DATE_FORMAT);
                    $this->user_model->update($id, $userdata);
                    $this->session->set_flashdata('success_message', array('User updated successfully.'));
                }else{
                    $userdata['password'] = $data['password'];
                    $userdata['createdBy'] = $this->session->userdata('admin_userid');
                    $userdata['createdDate'] = date(SQL_DATE_FORMAT);
                    $this->user_model->insert($userdata);
                    $this->session->set_flashdata('success_message', array('User added successfully.'));
                }

                redirect(admin_url() .$this->uri->segment(2). "/view");
            }
        }
        else{
            if($id != '')
            {
                $data = (array)$this->user_model->get($id);
               
                foreach ($data as $key => $value)
                {
                    $data[$key] = $value;
                }
                $data['page_title'] = 'Edit User';
                $data['breadcrumbs'] = array(array("name"=>"User","link"=>  site_url('admin/user/view')),array("name"=>"Edit User","link"=>"#"));
                $data['role_list'] = $this->role_model->get_all();
                $this->load->template('user/add',$data);       
            }
            else
            {
                $data['role_list'] = $this->role_model->get_all();
                $this->load->template('user/add',$data);       
            }   
        }
    }
    /*
    * Delete user 
    */

    function admin_delete($id = '')
    {
        $userdata['deletedBy'] = $this->session->userdata('admin_userid');
        $userdata['deletedDate'] = date(SQL_DATE_FORMAT);
        $userdata['isDeleted'] = 1;
        $this->user_model->update($id, $userdata);
        $this->session->set_flashdata('error_message', array('User deleted successfully.'));
    }



    public function ws_login(){
            $data = json_decode(file_get_contents('php://input'),true);
            $userName = $data['userName'];
            $password = $data['password'];

            $pwd = sha1($password); 
            $result = $this->min_check_login($userName,$pwd,ROLE_ADMIN,1);
            
            if(!empty($result)){
                $status = 1;
                $response = array('status'=>$status,'message'=>'Logged In Successfully.','userDetails'=>$result);
                echo json_encode($response);
            }else{
                $result = array();
                $status = 0;
                $response = array('status'=>$status,'message'=>'Email or password is Incorrect.','userDetails'=>$result);
                echo json_encode($response);
            }

    }

    public function ws_register(){
            $data = json_decode(file_get_contents('php://input'),true);
            $this->register_update('add','',$data);
    }

    public function ws_editProfile(){
            $data = json_decode(file_get_contents('php://input'),true);
            $this->register_update('edit',$data['userId'],$data);
    }

    function register_update($type,$id,$data){
            $userdata = array(
                    'roleId'=>ROLE_LABPERSON,
                   // 'gender'=>$data['gender'],
                   // 'dateOfBirth'=> date(SQL_DATE_FORMAT, strtotime($data['dateOfBirth'])),
                    'phoneNo'=>$data['phoneNo']
            );
            if($data['userName'] != ''){
                $isUserData = $this->user_model->check_userName($data['userName']);
                if($isUserData == 0){
                    $userdata['userName'] = $data['userName'];
                }else{
                    $status = 0;
                    $response = array('status'=>$status,'message'=>'User Name is already used.');
                    echo json_encode($response);
                    exit();
                }
            }
            if($data['email'] != ''){
                $isEmailData = $this->user_model->check_email($data['email']);
                if($isEmailData == 0){
                    $userdata['email'] = $data['email'];
                }else{
                    $status = 0;
                    $response = array('status'=>$status,'message'=>'Email is already used.');
                    echo json_encode($response);
                    exit();
                }
            }

            if($data['password'] != ''){
                $userdata['password'] = sha1($data['password']);
            }

            if($data['inputSerialKey'] != ''){
                $userdata['inputSerialKey'] = $data['inputSerialKey'];
            }else{
                $status = 0;
                $response = array('status'=>$status,'message'=>'Please Enter Input Serial Key.');
                echo json_encode($response);
                exit();
            }

            if($data['firstName'] != ''){
                $userdata['firstName'] = $data['firstName'];
            }

            if($data['lastName'] != ''){
                $userdata['lastName'] = $data['lastName'];
            }

            if($type == 'add'){
                $pinnumber = rand(1000,9999);
                $userdata['createdBy'] = 0;
                $userdata['pinnumber'] = $pinnumber;
                $userdata['createdDate'] = date(SQL_DATE_FORMAT);
                $this->user_model->insert($userdata);
                if($data['email'] != ''){
                    $this->load->library('email');
                    $strContent = '<p>Hello, [firstName] [lastName]</p><p>Your account is successfully created, find below detials.</p><p>Username : [userName]</p><p>Password : [password]</p><p>Email : [email]</p><p>Input Serial Number : [inputSerialKey]</p><p>Pin Number : [pinnumber]</p><p>&nbsp;</p><p>&nbsp;</p>';
                    
                    $arrValues = array(
                        'firstName' => $data['firstName'],
                        'lastName' => $data['lastName'],
                        'userName' => $data['userName'],
                        'password' => $data['password'],
                        'email' => $data['email'],
                        'inputSerialKey'=> $data['inputSerialKey'],
                        'pinnumber'=>$pinnumber
                    );

                    $arrOptions = array(
                        'to' => array($data['email']),
                        'subject' => 'Welcome to isoscope'
                     );
                    
                    $flag = $this->email->send_mail($strContent, $arrOptions, $arrValues);
                }
                $status = 1;
                $response = array('status'=>$status,'message'=>'Registration is added Successfully.');
                echo json_encode($response);
                exit();
            }else{
                $userdata['modifiedBy'] = 0;
                $userdata['modifiedDate'] = date(SQL_DATE_FORMAT);
                $this->user_model->update($id, $userdata);
                $status = 1;
                $response = array('status'=>$status,'message'=>'Profile is updated Successfully.');
                echo json_encode($response);
                exit();
            }
    }

    public function ws_forgotPassword(){
            $data = json_decode(file_get_contents('php://input'),true);
            if($data['email'] != ''){
                $isEmailData = $this->user_model->get_user_records_by_email($data['email']);
            
                if(!empty($isEmailData)){
                    $id = $isEmailData[0]->userId;
                    $this->load->library('email');
                    $strContent = '<p>Hello, [firstName] [lastName]</p><p>Please Enter below password.</p><p>&nbsp;</p><p>[password]</p><p>&nbsp;</p><p>&nbsp;</p>';
                    $pwd = generateRandomString(6);
                    $arrValues = array(
                        'firstName' => $isEmailData[0]->firstName,
                        'lastName' => $isEmailData[0]->lastName,
                        'password'=> $pwd
                    );

                    $arrOptions = array(
                        'to' => array($data['email']),
                        'subject' => 'Forgot Password'
                     );
                    
                    $flag = $this->email->send_mail($strContent, $arrOptions, $arrValues);
                    //echo $this->email->print_debugger();

                    if($flag){
                        $userdata['password'] = sha1($pwd);
                        $userdata['modifiedBy'] = 0;
                        $userdata['modifiedDate'] = date(SQL_DATE_FORMAT);
                        $this->user_model->update($id,$userdata);

                        $logofdata['emailoruserName'] = $data['email'];
                        $logofdata['password'] = sha1($pwd);
                        $logofdata['ip_address'] = $_SERVER['REMOTE_ADDR'];
                        $logofdata['message'] = 'Forget Password changed successfully.';
                        $logofdata['createdDate'] = date(SQL_DATE_FORMAT);
                        $this->logoflogin_model->insert($logofdata);

                        $status = 1;
                        $response = array('status'=>$status,'message'=>'Email is sent,check your in verified account.');
                        echo json_encode($response);
                        exit();  
                    }else{
                        $status = 0;
                        $response = array('status'=>$status,'message'=>'Email is not sent,Please again send your correct email.');
                        echo json_encode($response);
                        exit();
                    }
                }else{
                    $status = 0;
                    $response = array('status'=>$status,'message'=>'Email is not verified, Please enter correct email.');
                    echo json_encode($response);
                    exit();
                }
            }
    }

}