<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Patient extends MY_Controller {

    function __construct()
    {
        $this->accessRules = array(
            '*' => array('ws_patient_add','ws_patient_edit','ws_patient_view','ws_patient_search','ws_patient_delete','ws_patient_referalform'),
            '@' => array('admin_view', 'admin_add','admin_get','admin_delete'),
            '$' => array(),
            '#' => array()
        );
        parent::__construct();
        $this->load->model('campaign_model','campaign_model');
        $this->load->model('patient_model','patient_model');
        $this->load->model('user_model','user_model');
        $this->load->model('patienthistory_model','patienthistory_model');
        $this->load->model('referform_model','referform_model');
    }

    public function admin_view()
    {
        $this->load->add_js = array('custom_js/patient.js?v='.VERSION);
        $this->data['page_title'] = 'Manage Patient';
        $this->data['breadcrumbs'] = array(array("name"=>"Patient","link"=>"#"));
        
        $this->load->template('patient/list',$this->data);  
    }

    public function admin_get(){
        $data['records'] = $this->patient_model->get_patient_records();
        $arrResult = array();
        $this->load->view('patient/get', $data); 
    }

    /*
    * Add Ppatient
    */

    function admin_add($id='') 
    { 
        $data['page_title'] = 'Add Patient';
        $data['breadcrumbs'] = array(array("name"=>"Patient","link"=>  site_url('admin/patient/view')),array("name"=>"Add Patient","link"=>"#"));
        $data['patientId'] = '';
        $data['campaignId'] = '';
        $data['firstName'] = ''; 
        $data['lastName'] = ''; 
        $data['gender'] = ''; 
        $data['dateOfBirth'] = ''; 
        $data['race'] = ''; 
        $data['idNumber'] = '';   
        $data['disability'] = '';   
        $data['nationality'] = '';   
        $data['familyMemberName'] = '';   
        $data['familyMemeberPhoneNo'] = '';   
        $data['patientMedicalHistory'] = '';   
        $data['allergies'] = '';   
        $data['nearestHealthInstitution'] = '';   
        $data['location'] = '';  
        $data['phoneNo'] = '';
        $data['profilePic'] = '';

        if($this->input->post('eventSubmit') == 1){
            $data['patientId'] = $id;
            $data['campaignId'] = $this->input->post('campaignId');
            $data['firstName'] = $this->input->post('firstName');
            $data['lastName'] = $this->input->post('lastName');
            $data['gender'] = $this->input->post('gender');
            $data['dateOfBirth'] = date(SQL_DATE_FORMAT, strtotime($this->input->post('dateOfBirth')));
            $data['race'] = $this->input->post('race');
            $data['idNumber'] = $this->input->post('idNumber');
            $data['disability'] = $this->input->post('disability');
            $data['nationality'] = $this->input->post('nationality');
            $data['familyMemberName'] = $this->input->post('familyMemberName');
            $data['familyMemeberPhoneNo'] = $this->input->post('familyMemeberPhoneNo');
            $data['patientMedicalHistory'] = $this->input->post('patientMedicalHistory');
            $data['allergies'] = $this->input->post('allergies');
            $data['nearestHealthInstitution'] = $this->input->post('nearestHealthInstitution');
            $data['location'] = $this->input->post('location');
            $data['phoneNo'] = $this->input->post('phoneNo');
            $data['profilePichidden'] = $this->input->post('profilePichidden');

            $this->form_validation->set_rules('campaignId', 'Campaign', 'required');
            $this->form_validation->set_rules('firstName', 'First Name', 'required');
            $this->form_validation->set_rules('lastName', 'Last Name', 'required');
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            $this->form_validation->set_rules('dateOfBirth', 'Date Of Birth', 'required');
            $this->form_validation->set_rules('familyMemeberPhoneNo', 'Family Memeber PhoneNo', 'integer');
            $this->form_validation->set_rules('idNumber', 'Id Number', 'required');
            $this->form_validation->set_rules('location', 'Location', 'required');

            if ($this->form_validation->run() == FALSE) 
            {
                if($id != '')
                {
                    $data = (array)$this->patient_model->get_patient_record_by_id($id);
                    foreach ($data as $key => $value)
                    {
                        $data[$key] = $value;
                    }
                    $data['page_title'] = 'Edit Patient';
                    $data['breadcrumbs'] = array(array("name"=>"Patient","link"=>  site_url('admin/patient/view')),array("name"=>"Edit Patient","link"=>"#"));
                    $data['campaign_list'] = $this->campaign_model->get_all();
                    $this->load->template('patient/add/'.$id,$data);   
                }
                else
                {
                    $data['page_title'] = 'Add Patient';
                    $data['breadcrumbs'] = array(array("name"=>"Patient","link"=>  site_url('admin/patient/view')),array("name"=>"Add Patient","link"=>"#"));
                    $data['campaign_list'] = $this->campaign_model->get_all();
                    $this->load->template('patient/add', $data);
                }
            }
            else
            {
       
                    if (!empty($_FILES['profilePic']['name']))
                    {
                       // pre($_FILES);
                        $config['upload_path'] = UPLOAD_PATH_IMAGE;
                        $config['allowed_types'] =  '*';
                        $this->load->library('upload',$config);
                        /* 10-04-2015 -> Thumb Create begin */          
                        $options['image_resize']= IMAGE_RESIZE;
                        $options['source_folder']= UPLOAD_PATH_IMAGE;
                        $options['thumb_folder']= UPLOAD_THUMB_IMAGE_PATH_USER;
                        /* Thumb Create end     */
                        $field_name = "profilePic";
                         if(!$this->upload->do_upload($field_name,$options))
                            {
                                $error = $this->upload->display_errors();
                            }else{
                                $data_upload = array('upload_data' => $this->upload->data());
                                $newImageName = $data_upload['upload_data']['file_name'];
                                $data['profilePic'] = $newImageName;
                            }
                        }else{
                            $data['profilePic'] = $data['profilePichidden'];  
                        } 

                $userdata = array(
                    'roleId'=>ROLE_PATIENT,
                    'firstName'=>$data['firstName'],
                    'lastName'=>$data['lastName'],
                    'gender'=>$data['gender'],
                    'phoneNo'=>$data['phoneNo'],
                    'dateOfBirth'=> $data['dateOfBirth'],
                    'profilePic' => $data['profilePic']
                );

                $patientdata = array(
                    'campaignId'=>$data['campaignId'],
                    'race'=>$data['race'],
                    'idNumber'=>$data['idNumber'],
                    'disability'=>$data['disability'],
                    'nationality'=>$data['nationality'],
                    'familyMemberName'=>$data['familyMemberName'],
                    'familyMemeberPhoneNo'=>$data['familyMemeberPhoneNo'],
                    'patientMedicalHistory'=>$data['patientMedicalHistory'],
                    'allergies'=>$data['allergies'],
                    'nearestHealthInstitution'=>$data['nearestHealthInstitution'],
                    'location'=>$data['location'],
                    'date'=> date(SQL_DATE_FORMAT)
                );

                $patienthistorydata = array(
                    'firstName'=>$data['firstName'],
                    'lastName'=>$data['lastName'],
                    'gender'=>$data['gender'],
                    'phoneNo'=>$data['phoneNo'],
                    'dateOfBirth'=> date(SQL_DATE_FORMAT, strtotime($data['dateOfBirth'])),
                    'campaignId'=>$data['campaignId'],
                    'race'=>$data['race'],
                    'idNumber'=>$data['idNumber'],
                    'disability'=>$data['disability'],
                    'nationality'=>$data['nationality'],
                    'familyMemberName'=>$data['familyMemberName'],
                    'familyMemeberPhoneNo'=>$data['familyMemeberPhoneNo'],
                    'patientMedicalHistory'=>$data['patientMedicalHistory'],
                    'allergies'=>$data['allergies'],
                    'nearestHealthInstitution'=>$data['nearestHealthInstitution'],
                    'location'=>$data['location'], 
                    'date'=> date(SQL_DATE_FORMAT),
                    'createdDate'=> date(SQL_DATE_FORMAT)
                );

                if($id != ''){
                    $userdata['modifiedBy'] = $this->session->userdata('admin_userid');
                    $userdata['modifiedDate'] = date(SQL_DATE_FORMAT);
                    $this->user_model->update($id, $userdata);
                    $this->patient_model->update($id, $patientdata);
                    $patienthistorydata['patientId'] = $id;
                    $this->patienthistory_model->insert($patienthistorydata);
                    $this->session->set_flashdata('success_message', array('Patient updated successfully.'));
                }else{
                    $userdata['createdBy'] = $this->session->userdata('admin_userid');
                    $userdata['createdDate'] = date(SQL_DATE_FORMAT);
                    $userId = $this->user_model->insert($userdata);
                    $patienthistorydata['patientId']  = $patientdata['patientId'] = $userId;
                    $this->patient_model->insert($patientdata);
                    $this->patienthistory_model->insert($patienthistorydata);
                    $this->session->set_flashdata('success_message', array('Patient added successfully.'));
                }

                redirect(admin_url() .$this->uri->segment(2). "/view");
            }
        }
        else{
            if($id != '')
            {
                $data = (array)$this->patient_model->get_patient_record_by_id($id);
                $data = (array)$data[0];

                foreach ($data as $key => $value)
                {
                    $data[$key] = $value;
                }
                $data['page_title'] = 'Edit Patient';
                $data['breadcrumbs'] = array(array("name"=>"Patient","link"=>  site_url('admin/patient/view')),array("name"=>"Edit Patient","link"=>"#"));
                $data['campaign_list'] = $this->campaign_model->get_all();
                $this->load->template('patient/add',$data);       
            }
            else
            {
                $data['campaign_list'] = $this->campaign_model->get_all();
                $this->load->template('patient/add',$data);       
            }   
        }
    }
    /*
    * Delete patient 
    */

    function admin_delete($id = '')
    {
        $userdata['deletedBy'] = $this->session->userdata('admin_userid');
        $userdata['deletedDate'] = date(SQL_DATE_FORMAT);
        $userdata['isDeleted'] = 1;
        $this->user_model->update($id,$userdata);
        $this->session->set_flashdata('error_message', array('Patient deleted successfully.'));
    }

    function ws_patient_add(){
        $data = json_decode(file_get_contents('php://input'),true);
        $this->patient_update('add','',$data);
    }

    function ws_patient_edit(){
        $data = json_decode(file_get_contents('php://input'),true);
        $this->patient_update('edit',$data['patientId'],$data);
    }

     function patient_update($type,$id,$data){
                if(isset($data['facilityname'])){
                     $userdata = array(
                        'firstName'=>$data['firstName'],
                        'lastName'=>$data['lastName'],
                        'gender'=>$data['gender'],
                        'phoneNo'=>$data['cellno'],
                        'dateOfBirth'=> date(SQL_DATE_FORMAT, strtotime($data['dateOfBirth'])),
                    );

                    $patientdata = array(
                        'location'=>$data['location'],
                        'facilityname' => isset($data['facilityname'])?$data['facilityname']:"",
                        'referdate' =>isset($data['referdate'])?date(SQL_DATE_FORMAT, strtotime($data['referdate'])):"",
                        'refername' => isset($data['refername'])?$data['refername']:"",
                        'refercontact' => isset($data['refercontact'])?$data['refercontact']:"",
                        'reasonforreferral' => isset($data['reasonforreferral'])?$data['reasonforreferral']:""
                    );
                }else{
                    $userdata = array(
                        'roleId'=>ROLE_PATIENT,
                        'firstName'=>$data['firstName'],
                        'lastName'=>$data['lastName'],
                        'gender'=>$data['gender'],
                        'phoneNo'=>$data['cellno'],
                        'dateOfBirth'=> date(SQL_DATE_FORMAT, strtotime($data['dateOfBirth'])),
                    );
                   
                    $patientdata = array(
                        'campaignId'=>$data['campaignId'],
                        'race'=>$data['race'],
                        'idNumber'=>$data['idNumber'],
                        'disability'=>$data['disability'],
                        'nationality'=>$data['nationality'],
                        'familyMemberName'=>$data['familyMemberName'],
                        'familyMemeberPhoneNo'=>$data['familyMemeberPhoneNo'],
                        'patientMedicalHistory'=>$data['patientMedicalHistory'],
                        'allergies'=>$data['allergies'],
                        'nearestHealthInstitution'=>$data['nearestHealthInstitution'],
                        'location'=>$data['location'],
                        'reportFeedback'=>$data['reportFeedback'],
                        'facilityname' => isset($data['facilityname'])?$data['facilityname']:"",
                        'referdate' =>isset($data['referdate'])?date(SQL_DATE_FORMAT, strtotime($data['referdate'])):"",
                        'refername' => isset($data['refername'])?$data['refername']:"",
                        'refercontact' => isset($data['refercontact'])?$data['refercontact']:"",
                        'date'=>date(SQL_DATE_FORMAT, strtotime($data['date']))
                    );

                    $patienthistorydata = array(
                        'firstName'=>$data['firstName'],
                        'lastName'=>$data['lastName'],
                        'gender'=>$data['gender'],
                        'phoneNo'=>$data['cellno'],
                        'dateOfBirth'=> date(SQL_DATE_FORMAT, strtotime($data['dateOfBirth'])),
                        'campaignId'=>$data['campaignId'],
                        'race'=>$data['race'],
                        'idNumber'=>$data['idNumber'],
                        'disability'=>$data['disability'],
                        'nationality'=>$data['nationality'],
                        'familyMemberName'=>$data['familyMemberName'],
                        'familyMemeberPhoneNo'=>$data['familyMemeberPhoneNo'],
                        'patientMedicalHistory'=>$data['patientMedicalHistory'],
                        'allergies'=>$data['allergies'],
                        'nearestHealthInstitution'=>$data['nearestHealthInstitution'],
                        'location'=>$data['location'], 
                        'reportFeedback'=>$data['reportFeedback'],
                        'facilityname' => isset($data['facilityname'])?$data['facilityname']:"",
                        'referdate' =>isset($data['referdate'])?date(SQL_DATE_FORMAT, strtotime($data['referdate'])):"",
                        'refername' => isset($data['refername'])?$data['refername']:"",
                        'refercontact' => isset($data['refercontact'])?$data['refercontact']:"",
                        'reasonforreferral' => isset($data['reasonforreferral'])?$data['reasonforreferral']:"",
                        'date'=>date(SQL_DATE_FORMAT, strtotime($data['date'])),
                        'createdDate'=> date(SQL_DATE_FORMAT)
                    );     
                }
               
                $pId = $data['pId'];
                $flag = 0;
                if(isset($data['base64profilePic']) && $data['base64profilePic'] != ''){
                    /*$imgName = time().'.jpg';
                    $data = base64_decode(str_replace(' ', '+',str_replace('data:image/png;base64,', '',$data['base64profilePic'])));
                    file_put_contents(UPLOAD_PATH_IMAGE.$imgName, $data);
                    $flag = 1;*/
                    $imgName = time().'.jpg';
                    $img = $data['base64profilePic'];
                    $img = str_replace('data:image/png;base64,', '', $img);
                    $img = str_replace(' ', '+', $img);
                    $data = base64_decode($img);
                    file_put_contents(UPLOAD_PATH_IMAGE.$imgName, $data);
                    $flag = 1;
                }

                if($flag == 1){
                    $userdata['profilePic'] = $imgName;
                }


                if($id != ''){
                    $userdata['modifiedBy'] = $pId;
                    $userdata['modifiedDate'] = date(SQL_DATE_FORMAT);
                    $this->user_model->update($id, $userdata);
                    $this->patient_model->update($id, $patientdata);
                    $patientId = $patienthistorydata['patientId'] = $id;
                    if(!isset($data['facilityname'])){
                        $this->patienthistory_model->insert($patienthistorydata);
                    }
                    $response = array('status'=>1,'message'=>'Patient updated successfully.','patientId'=>$patientId);
                    echo json_encode($response);
                }else{
                    $userdata['createdBy'] = $pId;
                    $userdata['createdDate'] = date(SQL_DATE_FORMAT);
                    $userId = $this->user_model->insert($userdata);
                    $patientId = $patienthistorydata['patientId']  = $patientdata['patientId'] = $userId;
                    $this->patient_model->insert($patientdata);
                    if(!isset($data['facilityname'])){
                        $this->patienthistory_model->insert($patienthistorydata);
                    }
                    $response = array('status'=>1,'message'=>'Patient added successfully.','patientId'=>$patientId);
                    echo json_encode($response);
                }
    }

    function ws_patient_view(){
        $data = json_decode(file_get_contents('php://input'),true);
        $patientdata = (array)$this->patient_model->get_patient_record_by_id($data['patientId']);
       
        if(!empty($patientdata)){
            foreach ($patientdata as $key => $value)
            {
                $patientdata[$key] = $value;
            }

            $patientdata = (array)$patientdata[0];

            $response = array('status'=>1,'message'=>'Patient view successfully.','patientData'=>$patientdata);
            echo json_encode($response);
        }else{
            $response = array('status'=>0,'message'=>'Patient is not available.');
            echo json_encode($response);
        }
      
    }

    function ws_patient_search(){
        $data = json_decode(file_get_contents('php://input'),true);
        $patientdata = (array)$this->patient_model->patient_search($data['search'],$data['pageIndex'],$data['pageSize'],$data['campaignId']);
        if(!empty($patientdata)){
            $total_data = $this->patient_model->get_patient_records();
            $total_records = count($total_data);
            $max_page_index = ceil($total_records/$data['pageSize']);
            foreach ($patientdata as $key => $value) {
                if($value->profilePic != ''){
                     $value->profilePic = UPLOAD_IMAGE_URL.$value->profilePic;
                 }
            }
            $response = array('status'=>1,'message'=>'Patient view successfully.','max_page_index'=>$max_page_index,'patientData'=>$patientdata);
            echo json_encode($response);
        }else{
            $response = array('status'=>0,'message'=>'Patient is not available.');
            echo json_encode($response);
        }
    }
 
    function ws_patient_delete(){
        $data = json_decode(file_get_contents('php://input'),true);
        $userdata['deletedBy'] = $data['pId'];
        $userdata['deletedDate'] = date(SQL_DATE_FORMAT);
        $userdata['isDeleted'] = 1;
        $this->user_model->update($data['patientId'],$userdata);
        $patientdata = (array)$this->patient_model->get_patient_record_by_id($data['patientId']);
        $response = array('status'=>1,'message'=>'Patient deleted successfully.');
        echo json_encode($response);
    }

    function ws_patient_referalform(){
        $data = json_decode(file_get_contents('php://input'),true);

        $referdata = array(
                    'q1'=>$data['q1'],
                    'q1a'=>$data['q1a'],
                    'q1b'=>$data['q1b'],
                    'q2a'=>$data['q2a'],
                    'q2b'=>$data['q2b'],
                    'q3'=>$data['q3'],
                    'q4'=>$data['q4'],
                    'q5'=>$data['q5'],
                    'q6'=>$data['q6']
                );

        $this->patient_model->update($data['patientId'],$referdata);   
        $response = array('status'=>1,'message'=>'refer form updated successfully.');
        echo json_encode($response);
    }
}