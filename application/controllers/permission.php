<?php

/**
 * @author Nidhi Barhate
 * @copyright 2016
 */
class Permission extends MY_Controller {

    function __construct() {
        $this->accessRules = array(
            '*' => array('admin_login', 'admin_check_login' ,'admin_email_reset','admin_update_password','admin_forgot_Password','admin_send_password','login','forgot_password','reset_password','add','index','min_check_login','ws_login','ws_register', 'ws_forgotpass'),
            '@' => array('admin_logout', 'admin_view', 'admin_add','admin_get' ,'admin_edit','admin_reset_password','admin_delete','admin_list_permission','admin_get_permission' ),
            '$' => array(),
            '#' => array('ws_changepass','ws_update')
            );
        parent::__construct();

        $this->load->model('permissionmodel','permissionmodel');
    }

    
    /*
    * Permission View
    */
    
    function admin_view()
    {
        $this->load->add_js = array('custom_js/permission.js?v='.VERSION);
        $this->data['page_title'] = 'Manage Permission';
        $this->data['breadcrumbs'] = array(array("name"=>"Manage","link"=>"#"),array("name"=>"Manage Role","link"=>"#"));
        

        $this->data['permission'] = $this->permissionmodel->select();
        
        $this->load->template('permisssion/list',$this->data);  
    }

    /*
    * Permission Data
    */
    
    function admin_get()
    {
        $this->load->model('permissionmodel');
        
        $this->permissionmodel->countQuery = 1;
        
        $sortBy = 'permissionName';
        
        if($this->input->post('sortBy') != '')
        {
            $sortBy = $this->input->post('sortBy');
        }
        $this->permissionmodel->sortBy = $sortBy;
        
        $sortOrder = 'ASC';
        if($this->input->post('sortOrder') != '')
        {
            $sortOrder = $this->input->post('sortOrder');
        }
        $this->permissionmodel->sortOrder = $sortOrder;
        
        $page = 1;
        if($this->input->post('page') != '')
        {
            $page = $this->input->post('page');
        }
        $this->permissionmodel->page = $page;
        
        $offset = 0;
        if($this->input->post('offset') != '')
        {
            $offset = $this->input->post('offset');
        }
        $this->permissionmodel->offset = $offset;
        $this->permissionmodel->joinPermission = 1;

        if($this->input->post('permissionId') != '')
        {
            $this->permissionmodel->permissionId = $this->input->post('permissionId');
        }
        if($this->input->post('permissionName') != '')
        {
            $this->permissionmodel->permissionName = trim($this->input->post('permissionName'));
        }
        
        
        $this->permissionmodel->record_per_page = $this->input->post('record_per_page');
        $arrResult['result'] = $this->permissionmodel->get_list();
       //echo "<pre>";print_r($arrResult['result']);die();
        $this->load->view('permisssion/get', $arrResult); 
    }
     /*
    * Add Permission
    */
    function admin_add($id='') 
    { 
        //$data['title'] = 'Add Permission';
        $data['page_title'] = 'Add Permission';
        $data['breadcrumbs'] = array(array("name"=>"Manage","link"=>"#"),array("name"=>"Manage Permission","link"=>  site_url('admin/permission/view')),array("name"=>"Add Permission","link"=>"#"));
        
        $data['permissionId'] = '';
        $data['permissionName'] = '';
        $data['permissionFunction'] = '';
        $this->load->model('permissionmodel');
        

        if($this->input->post('btnsubmit') == 1)
        {
            
            $data['permisssionId'] = $id;
            $data['permissionName'] = $this->input->post('permissionName');
            $data['permissionFunction'] = $this->input->post('permissionFunction');
            $editerr = 0;
            $this->form_validation->set_rules('permissionName','Permission Name','required|is_unique[permission.permissionName]');
            $this->form_validation->set_rules('permissionFunction','Permission Function','required|is_unique[permission.permissionName]');
            //echo validation_errors();die();
            if($id != '')
            {
                $permissionresult = $this->permissionmodel->selectnotbyid($id);
                
                foreach ($permissionresult as $value)
                {
                    if($data['permissionName'] == $value->permissionName)
                    {
                        $editerr = 1;
                    }
                }   
            }
            
            if ($this->form_validation->run() == FALSE && $id == '') 
            {
                //echo validation_errors();
                //r($data);die();
                $this->load->template('permisssion/profile', $data);
                
            }
            elseif($editerr == 1 && $id != '')
            {

                $data = $this->permissionmodel->selectbyid($id);
                foreach ($data['resultarr']['0'] as $key => $value)
                {
                    $data[$key] = $value;
                }
                
                $this->load->template('permisssion/profile',$data);   
            }
            else
            {

                if($id != '')
                {
                    $permissiondata = array(
                        'permissionName'=>$data['permissionName'],
                        'permissionFunction'=>$data['permissionFunction'],
                        'modifiedBy' => $this->session->userdata['admin_userid'],
                        'modifiedDate' => date("Y-m-d h:i:s")
                    );
                     $this->permissionmodel->insert_update($permissiondata,$tablename="permission",$columnname="permissionId",$id);
                    $this->session->set_flashdata('success_message',array('Permission updated successfully.'));
                }
                else
                {
                      $permissiondata = array(
                        'permissionName'=>$data['permissionName'],
                        'permissionFunction'=>$data['permissionFunction'],
                        'createdBy' => $this->session->userdata['admin_userid'],
                        'createdDate' => date("Y-m-d h:i:s")
                    );
                    $this->permissionmodel->insert_update($permissiondata,$tablename="permission",$columnname="permissionId",$id);
                    $permissionid = $this->db->insert_id();
                    $this->permissionmodel->insert_rolePermission($permissionid);
                    $this->session->set_flashdata('success_message',array('Permission added successfully.'));
                }
                redirect(admin_url() .$this->uri->segment(2). "/view");
            }
        }
        else
        {
            if($id != '')
            {
                $data = $this->permissionmodel->selectbyid($id);
                foreach ($data['resultarr']['0'] as $key => $value)
                {
                    $data[$key] = $value;
                }
                //$data['title'] = 'Add Permission';
                $data['page_title'] = 'Edit Permission';
                $data['breadcrumbs'] = array(array("name"=>"Manage","link"=>"#"),array("name"=>"Manage Permission","link"=>  site_url('admin/permission/view')),array("name"=>"Edit Permission","link"=>"#"));
                $this->load->template('permisssion/profile',$data);       
            }
            else
            {
                $this->load->template('permisssion/profile',$data);       
            }   
        }
    }
    /*
    * Delete Permission 
    */

    function admin_delete($id = '')
    {
        $data['deletedBy'] = $this->session->userdata['admin_userid'];
        
        $this->load->model('permissionmodel');

        if(!empty($this->input->post['sel_arr']))
        {
            $sel_arr = $this->input->post('sel_arr');
        }
        else
        {
            $this->permissionmodel->delete($this->input->post('id'),$data);
        }
    }

    /** **/
    /** User Functions starts here. **/
    function index()
    {
        $this->load->model('permissionmodel');
        $this->load->template('templates/index');
    }
    
    /** User Permission Functions starts here. **/
    function admin_list_permission($permissionid='')
    {
        $this->load->model('permissionmodel');
        $data['permission_permission'] = $this->permissionmodel->selectPermissionbyid($permissionid);
        $this->load->view('permisssion/list_permision', $data); 
    }
    /*
    * Permission Permission Data
    */
    function admin_get_permission()
    {
        $this->load->model('permissionmodel');
        
        $this->permissionmodel->countQuery = 1;
        
        $sortBy = 'permissionName';
        
        if($this->input->post('sortBy') != '')
        {
            $sortBy = $this->input->post('sortBy');
        }
        $this->permissionmodel->sortBy = $sortBy;
        
        $sortOrder = 'ASC';
        if($this->input->post('sortOrder') != '')
        {
            $sortOrder = $this->input->post('sortOrder');
        }
        $this->permissionmodel->sortOrder = $sortOrder;
        
        $page = 1;
        if($this->input->post('page') != '')
        {
            $page = $this->input->post('page');
        }
        $this->permissionmodel->page = $page;
        
        $offset = 0;
        if($this->input->post('offset') != '')
        {
            $offset = $this->input->post('offset');
        }
        $this->permissionmodel->offset = $offset;
        $this->permissionmodel->joinPermission = 1;

        if($this->input->post('permissionId') != '')
        {
            $this->permissionmodel->permissionId = $this->input->post('permissionId');
        }
        if($this->input->post('permissionName') != '')
        {
            $this->permissionmodel->permissionName = trim($this->input->post('permissionName'));
        }
        
        
        $this->permissionmodel->record_per_page = $this->input->post('record_per_page');
        $arrResult['result'] = $this->permissionmodel->get_list();
       //echo "<pre>";print_r($arrResult['result']);die();
        $this->load->view('permisssion/get_permission', $arrResult); 
    }
}
?>
