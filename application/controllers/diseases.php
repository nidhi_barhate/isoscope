<?php
class Diseases extends MY_Controller {

    function __construct() {
        $this->accessRules = array(
            '*' => array(),
            '@' => array('admin_view', 'admin_add','admin_get','admin_delete'),
            '$' => array(),
            '#' => array()
            );
        parent::__construct();
        $this->load->model('diseases_model','diseases_model');
    }

    
    /*
    * diseases View
    */
    
    function admin_view()
    {
        $this->load->add_js = array('custom_js/diseases.js?v='.VERSION);
        $this->data['page_title'] = 'Manage Diseases';
        $this->data['breadcrumbs'] = array(array("name"=>"Diseases","link"=>"#"));
        
        $this->load->template('diseases/list',$this->data);  
    }

    /*
    * diseases Data
    */
    
    function admin_get()
    {
        $data['records'] = $this->diseases_model->get_all();
        $arrResult = array();
        $this->load->view('diseases/get', $data); 
    }
    
    /*
    * Add diseases
    */
    function admin_add($id='') 
    { 
        $data['page_title'] = 'Add Diseases';
        $data['breadcrumbs'] = array(array("name"=>"Diseases","link"=>  site_url('admin/diseases/view')),array("name"=>"Add Diseases","link"=>"#"));
        $data['diseasesId'] = '';
        $data['diseasesName'] = '';
        $data['diseasesShortCode'] = '';

        if($this->input->post('eventSubmit') == 1){
            
            $data['diseasesId'] = $id;
            $data['diseasesName'] = $this->input->post('diseasesName');
            $data['diseasesShortCode'] = $this->input->post('diseasesShortCode');

            $this->form_validation->set_rules('diseasesName', 'Diseases Name', 'required');
            $this->form_validation->set_rules('diseasesShortCode', 'Diseases Short Code', 'required');
            if ($this->form_validation->run() == FALSE) 
            {
                
                if($id != '')
                {
                    $data = (array)$this->diseases_model->get($id);
                    foreach ($data as $key => $value)
                    {
                        $data[$key] = $value;
                    }
                    $data['page_title'] = 'Edit Diseases';
                    $data['breadcrumbs'] = array(array("name"=>"Diseases","link"=>  site_url('admin/diseases/view')),array("name"=>"Edit Diseases","link"=>"#"));
                    $this->load->template('diseases/add/'.$id,$data);   
                }
                else
                {
                    $data['page_title'] = 'Add Diseases';
                    $data['breadcrumbs'] = array(array("name"=>"Diseases","link"=>  site_url('admin/diseases/view')),array("name"=>"Add Diseases","link"=>"#"));
                    $this->load->template('diseases/add', $data);
                }
            }
            else
            {
                $diseasesdata = array(
                    'diseasesName'=> $data['diseasesName'],
                    'diseasesShortCode'=>$data['diseasesShortCode'],
                );

                if($id != ''){
                    $diseasesdata['modifiedBy'] = $this->session->userdata('admin_userid');
                    $diseasesdata['modifiedDate'] = date(SQL_DATE_FORMAT);
                    $this->diseases_model->update($id, $diseasesdata);
                    $this->session->set_flashdata('success_message', array('Diseases updated successfully.'));
                }else{
                    $diseasesdata['createdBy'] = $this->session->userdata('admin_userid');
                    $diseasesdata['createdDate'] = date(SQL_DATE_FORMAT);
                    $this->diseases_model->insert($diseasesdata);
                    $this->session->set_flashdata('success_message', array('Diseases added successfully.'));
                }

                redirect(admin_url() .$this->uri->segment(2). "/view");
            }
        }
        else{
            if($id != '')
            {
                $data = (array)$this->diseases_model->get($id);
               
                foreach ($data as $key => $value)
                {
                    $data[$key] = $value;
                }
                $data['page_title'] = 'Edit Diseases';
                $data['breadcrumbs'] = array(array("name"=>"Diseases","link"=>  site_url('admin/diseases/view')),array("name"=>"Edit Diseases","link"=>"#"));
               
                $this->load->template('diseases/add',$data);       
            }
            else
            {
                $this->load->template('diseases/add',$data);       
            }   
        }
    }
    /*
    * Delete diseases 
    */

    function admin_delete($id = '')
    {
        $diseasesdata['deletedBy'] = $this->session->userdata('admin_userid');
        $diseasesdata['deletedDate'] = date(SQL_DATE_FORMAT);
        $diseasesdata['isDeleted'] = 1;
        $this->diseases_model->update($id, $diseasesdata);
        $this->session->set_flashdata('error_message', array('Diseases deleted successfully.'));
    }
}
?>
