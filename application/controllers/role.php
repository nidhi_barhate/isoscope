<?php
class Role extends MY_Controller {

    function __construct() {
        $this->accessRules = array(
            '*' => array(),
            '@' => array('admin_view', 'admin_add','admin_get','admin_delete'),
            '$' => array(),
            '#' => array()
            );
        parent::__construct();
        $this->load->model('role_model','role_model');
    }

    
    /*
    * Role View
    */
    
    function admin_view()
    {
        //$data['title'] = 'Manage E-mail Template';
        $this->load->add_js = array('custom_js/role.js?v='.VERSION);
        $this->data['page_title'] = 'Manage Role';
        $this->data['breadcrumbs'] = array(array("name"=>"Role","link"=>"#"));
        
        $this->load->template('role/list',$this->data);  
    }

    /*
    * Role Data
    */
    
    function admin_get()
    {
        $data['records'] = $this->role_model->get_all();
        $arrResult = array();
        $this->load->view('role/get', $data); 
    }
    
    /*
    * Add Role
    */
    function admin_add($id='') 
    { 
        $data['page_title'] = 'Add Role';
        $data['breadcrumbs'] = array(array("name"=>"Role","link"=>  site_url('admin/manage_role/view')),array("name"=>"Add Role","link"=>"#"));
        $data['roleId'] = '';
        $data['roleName'] = '';
        $data['roleTitle'] = '';

        if($this->input->post('eventSubmit') == 1){
            
            $data['manage_roleId'] = $id;
            $data['roleName'] = str_replace(' ','',strtolower($this->input->post('roleName')));
            $data['roleTitle'] = $this->input->post('roleTitle');

            if($id != ''){
                $sdata = (array)$this->role_model->get($id);
                foreach ($sdata as $key => $value){
                    $sdata[$key] = $value;
                }
                
                if($data['roleName'] == $sdata['roleName'] && $data['roleTitle'] == $sdata['roleTitle']){
                    $this->session->set_flashdata('error_message', array('The Role Name field must contain a unique value.'));
                    redirect(admin_url() .$this->uri->segment(2). "/add/".$id);
                }
            }

            $this->form_validation->set_rules('roleName', 'Role Name', 'required');
            $this->form_validation->set_rules('roleTitle', 'Role Title', 'required');
            if ($this->form_validation->run() == FALSE) 
            {
                
                if($id != '')
                {
                    $data = (array)$this->role_model->get($id);
                    foreach ($data as $key => $value)
                    {
                        $data[$key] = $value;
                    }
                    $data['page_title'] = 'Edit Role';
                    $data['breadcrumbs'] = array(array("name"=>"Role","link"=>  site_url('admin/role/view')),array("name"=>"Edit Role","link"=>"#"));
                    $this->load->template('role/add/'.$id,$data);   
                }
                else
                {
                    $data['page_title'] = 'Add Role';
                    $data['breadcrumbs'] = array(array("name"=>"Role","link"=>  site_url('admin/role/view')),array("name"=>"Add Role","link"=>"#"));
                    $this->load->template('role/add', $data);
                }
            }
            else
            {
                $roledata = array(
                    'roleName'=> $data['roleName'],
                    'roleTitle'=>$data['roleTitle'],
                );

                if($id != ''){
                    $roledata['modifiedBy'] = $this->session->userdata('admin_userid');
                    $roledata['modifiedDate'] = date(SQL_DATE_FORMAT);
                    $this->role_model->update($id, $roledata);
                    $this->session->set_flashdata('success_message', array('Role updated successfully.'));
                }else{
                    $roledata['roleShortCode'] =  str_replace(' ','',strtolower($data['roleName']));
                    $roledata['createdBy'] = $this->session->userdata('admin_userid');
                    $roledata['createdDate'] = date(SQL_DATE_FORMAT);
                    $this->role_model->insert($roledata);
                    $this->session->set_flashdata('success_message', array('Role added successfully.'));
                }

                redirect(admin_url() .$this->uri->segment(2). "/view");
            }
        }
        else{
            if($id != '')
            {
                $data = (array)$this->role_model->get($id);
               
                foreach ($data as $key => $value)
                {
                    $data[$key] = $value;
                }
                $data['page_title'] = 'Edit Role';
                $data['breadcrumbs'] = array(array("name"=>"Role","link"=>  site_url('admin/role/view')),array("name"=>"Edit Role","link"=>"#"));
               
                $this->load->template('role/add',$data);       
            }
            else
            {
                $this->load->template('role/add',$data);       
            }   
        }
    }
    /*
    * Delete Role 
    */

    function admin_delete($id = '')
    {
        $roledata['deletedBy'] = $this->session->userdata('admin_userid');
        $roledata['deletedDate'] = date(SQL_DATE_FORMAT);
        $roledata['isDeleted'] = 1;
        $this->role_model->update($id, $roledata);
        $this->session->set_flashdata('error_message', array('Role deleted successfully.'));
    }
}
?>
