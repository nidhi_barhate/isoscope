<?php
class Campaign extends MY_Controller {

    function __construct() {
        $this->accessRules = array(
            '*' => array('ws_campaign_add','ws_campaign_edit','ws_campaign_view','ws_campaign_search'),
            '@' => array('admin_view', 'admin_add','admin_get','admin_delete'),
            '$' => array(),
            '#' => array()
            );
        parent::__construct();
        $this->load->model('campaign_model','campaign_model');
    }

    
    /*
    * campaign View
    */
    
    function admin_view()
    {
        $this->load->add_js = array('custom_js/campaign.js?v='.VERSION);
        $this->data['page_title'] = 'Manage Campaign';
        $this->data['breadcrumbs'] = array(array("name"=>"Campaign","link"=>"#"));
        
        $this->load->template('campaign/list',$this->data);  
    }

    /*
    * campaign Data
    */
    
    function admin_get()
    {
        $data['records'] = $this->campaign_model->get_all();
        $arrResult = array();
        $this->load->view('campaign/get', $data); 
    }
    
    /*
    * Add campaign
    */
    function admin_add($id='') 
    { 
        $data['page_title'] = 'Add Campaign';
        $data['breadcrumbs'] = array(array("name"=>"Campaign","link"=>  site_url('admin/campaign/view')),array("name"=>"Add Campaign","link"=>"#"));
        $data['campaignId'] = '';
        $data['campaignName'] = '';
        $data['location'] = '';

        if($this->input->post('eventSubmit') == 1){
            
            $data['campaignId'] = $id;
            $data['campaignName'] = $this->input->post('campaignName');
            $data['location'] = $this->input->post('location');

            $this->form_validation->set_rules('campaignName', 'Campaign Name', 'required');
            $this->form_validation->set_rules('location', 'Location', 'required');
            if ($this->form_validation->run() == FALSE) 
            {
                
                if($id != '')
                {
                    $data = (array)$this->campaign_model->get($id);
                    foreach ($data as $key => $value)
                    {
                        $data[$key] = $value;
                    }
                    $data['page_title'] = 'Edit Campaign';
                    $data['breadcrumbs'] = array(array("name"=>"Campaign","link"=>  site_url('admin/campaign/view')),array("name"=>"Edit Campaign","link"=>"#"));
                    $this->load->template('campaign/add/'.$id,$data);   
                }
                else
                {
                    $data['page_title'] = 'Add Campaign';
                    $data['breadcrumbs'] = array(array("name"=>"Campaign","link"=>  site_url('admin/campaign/view')),array("name"=>"Add Campaign","link"=>"#"));
                    $this->load->template('campaign/add', $data);
                }
            }
            else
            {
                $campaigndata = array(
                    'campaignName'=> $data['campaignName'],
                    'location'=>$data['location'],
                );

                if($id != ''){
                    $campaigndata['modifiedBy'] = $this->session->userdata('admin_userid');
                    $campaigndata['modifiedDate'] = date(SQL_DATE_FORMAT);
                    $this->campaign_model->update($id, $campaigndata);
                    $this->session->set_flashdata('success_message', array('Campaign updated successfully.'));
                }else{
                    $campaigndata['createdBy'] = $this->session->userdata('admin_userid');
                    $campaigndata['createdDate'] = date(SQL_DATE_FORMAT);
                    $this->campaign_model->insert($campaigndata);
                    $this->session->set_flashdata('success_message', array('Campaign added successfully.'));
                }

                redirect(admin_url() .$this->uri->segment(2). "/view");
            }
        }
        else{
            if($id != '')
            {
                $data = (array)$this->campaign_model->get($id);
               
                foreach ($data as $key => $value)
                {
                    $data[$key] = $value;
                }
                $data['page_title'] = 'Edit Campaign';
                $data['breadcrumbs'] = array(array("name"=>"Campaign","link"=>  site_url('admin/campaign/view')),array("name"=>"Edit Campaign","link"=>"#"));
               
                $this->load->template('campaign/add',$data);       
            }
            else
            {
                $this->load->template('campaign/add',$data);       
            }   
        }
    }
    /*
    * Delete campaign 
    */

    function admin_delete($id = '')
    {
        $campaigndata['deletedBy'] = $this->session->userdata('admin_userid');
        $campaigndata['deletedDate'] = date(SQL_DATE_FORMAT);
        $campaigndata['isDeleted'] = 1;
        $this->campaign_model->update($id, $campaigndata);
        $this->session->set_flashdata('error_message', array('Campaign deleted successfully.'));
    }

    function ws_campaign_add(){
        $data = json_decode(file_get_contents('php://input'),true);
        $this->campaign_update('add','',$data);
    }

    function ws_campaign_edit(){
        $data = json_decode(file_get_contents('php://input'),true);
        $this->campaign_update('edit',$data['campaignId'],$data);
    }

    function campaign_update($type,$id,$data){
                $campaigndata = array(
                    'campaignName'=> $data['campaignName'],
                    'location'=>$data['location'],
                );

                if($id != ''){
                    $campaigndata['modifiedBy'] = $data['pId'];;
                    $campaigndata['modifiedDate'] = date(SQL_DATE_FORMAT);
                    $this->campaign_model->update($id, $campaigndata);
                    $response = array('status'=>1,'message'=>'Campaign updated successfully.','campaignId'=>$id);
                    echo json_encode($response);
                }else{
                    $campaigndata['createdBy'] = $data['pId'];
                    $campaigndata['createdDate'] = date(SQL_DATE_FORMAT);
                    $id = $this->campaign_model->insert($campaigndata);
                    $response = array('status'=>1,'message'=>'Campaign added successfully.','campaignId'=>$id);
                    echo json_encode($response);
                }
    }

    function ws_campaign_view(){
        $data = json_decode(file_get_contents('php://input'),true);
        $campaigndata = (array)$this->campaign_model->get($data['campaignId']);
       
        if(!empty($campaigndata)){
            $response = array('status'=>1,'message'=>'Campaign view successfully.','campaignData'=>$campaigndata);
            echo json_encode($response);
        }else{
            $response = array('status'=>0,'message'=>'Campaign is not available.');
            echo json_encode($response);
        }
      
    }

    function ws_campaign_search(){
        $data = json_decode(file_get_contents('php://input'),true);
        $campaignData = (array)$this->campaign_model->campaign_search($data['search'],$data['pageIndex'],$data['pageSize']);
        if(!empty($campaignData)){
            $response = array('status'=>1,'message'=>'Campaign view successfully.','campaignData'=>$campaignData);
            echo json_encode($response);
        }else{
            $response = array('status'=>0,'message'=>'Campaign is not available.');
            echo json_encode($response);
        }
    }
}
?>
