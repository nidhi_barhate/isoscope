<?php

function asset_url() {
    return base_url() . 'assets/';
}

function admin_url() {
    return base_url() . 'admin/';
}
function user_url() {
    return base_url() . 'user/';
}

function pr($array) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function pre($array) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";
    exit;
}

function ex($value)
{
    echo $value;
    exit;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

?>
 
