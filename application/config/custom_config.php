<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
* Custom Constant
*/
define('ROLE_ADMIN',1);
define('ROLE_PATIENT',2);
define('ROLE_PATHOLOGIST',3);
define('ROLE_LABPERSON',4);
define('VERSION',1);
define('DATE_FORMAT','d-m-Y');
define('SQL_DATE_FORMAT','Y-m-d H:i:s');

define('UPLOAD_PATH', FCPATH . 'assets/uploads/');    
$config['wordwraUPLOAD_PATHp'] = TRUE;
define('CDN_UPLOAD','0');

define('ALLOWED_TYPES_VIDEO', 'avi|flv|wmv|mp4|mp3');

define('RECORDS_PER_PAGE_OPTIONS', '10,15,20,25');
define('ROWS_PER_PAGE', '10');
define('ALLOWED_TYPES_IMAGE', 'jpg|png|gif|bmp|JPG|jpeg');

// Mail Settings
define('PROTOCOL', 'smtp');
define('SMTP_HOST', 'smtp.1und1.de');
define('SMTP_PORT', '587');
define('SMTP_USER', 'nidhi.b@msp-group.co.uk');
define('SMTP_PASS', 'msp@2015');
define('SMTP_TIMEOUT', '10');
define('SMTP_CRYPTO', 'tls');
define('MAILTYPE', 'html');
define('CHARSET', 'iso-8859-1');
define('MAIL_FROMNAME', 'ISOSCOPE');
define('MAIL_FROMEMAIL', 'admin@isoscope.com');
define('WORDWRAP', 'TRUE');

define('INSIDER_POSTS', '1');
define('PRODUCTS_SCREEN_POSTS', '4');
define('POST_TO_SHARE', '3');
define('POST_MEDIA', '2');
define('MEDIA_POST_ID', '1');

// Absolute Paths
define('ASSETS_PATH', FCPATH .'assets/');
define('UPLOADS_PATH', ASSETS_PATH . 'uploads/');

// URLs
define('ASSETS_URL', SITE_URL .'assets/');
define('UPLOADS_URL', ASSETS_URL . 'uploads/');
define('IMG_URL', ASSETS_URL .'img/');

/* Insider post begin */
define('INSIDER_POSTS_PATH', UPLOADS_PATH . 'insider_posts/');
define('INSIDER_POSTS_THUMB_PATH',INSIDER_POSTS_PATH.'thumb/');
define('INSIDER_POSTS_URL', UPLOADS_URL . 'insider_posts/');
define('INSIDER_POSTS_THUMB_URL', INSIDER_POSTS_URL . 'thumb/');
/* Insider post end */

/* products screen posts begin */
define('PRODUCTS_SCREEN_POSTS_PATH', UPLOADS_PATH . 'products_screen/');
define('PRODUCTS_SCREEN_POSTS_THUMB_PATH',PRODUCTS_SCREEN_POSTS_PATH.'thumb/');
define('PRODUCTS_SCREEN_POSTS_URL', UPLOADS_URL . 'products_screen/');
define('PRODUCTS_SCREEN_POSTS_THUMB_URL', PRODUCTS_SCREEN_POSTS_URL . 'thumb/');
/* products screen posts end */

/* Post to share begin */
define('POST_TO_SHARE_POSTS_PATH', UPLOADS_PATH . 'post_to_share/');
define('POST_TO_SHARE_THUMB_PATH',POST_TO_SHARE_POSTS_PATH.'thumb/');
define('POST_TO_SHARE_POSTS_URL', UPLOADS_URL . 'post_to_share/');
define('POST_TO_SHARE_THUMB_URL', POST_TO_SHARE_POSTS_URL . 'thumb/');
/* Post to share end */

/* Media Begin  */
define('POST_MEDIA_PATH', UPLOADS_PATH . 'media/');
define('POST_MEDIA_THUMB_PATH',POST_MEDIA_PATH.'thumb/');
define('POST_MEDIA_URL', UPLOADS_URL . 'media/');
define('POST_MEDIA_THUMB_URL', POST_MEDIA_URL . 'thumb/');
/* Media End */
//URL FOR THE BANNER

define('UPLOAD_PATH_BANNER_IMAGE',FCPATH.'assets/images/');



//URL FOR THE USER PROFILE PIC
define('UPLOAD_PATH_IMAGE',UPLOADS_PATH.'user/');
define('UPLOAD_THUMB_IMAGE_PATH_USER',UPLOAD_PATH_IMAGE.'thumb/');
define('UPLOAD_IMAGE_URL',UPLOADS_URL.'user/');
define('UPLOAD_THUMB_IMAGE_URL_USER',UPLOAD_IMAGE_URL.'thumb/');
define('UPLOAD_PATH_TEASERS_IMAGE',UPLOADS_PATH.'teasers_images/');
define('UPLOAD_PATH_TN_IMAGE',UPLOADS_PATH.'nutri_tips/');
define('UPLOAD_THUMB_IMAGE_PATH_TN',UPLOAD_PATH_TN_IMAGE.'thumb/');
define('UPLOAD_IMAGE_TN_URL',UPLOADS_URL.'nutri_tips/');
define('UPLOAD_THUMB_IMAGE_URL_TN',UPLOAD_IMAGE_TN_URL.'thumb/');

//URL FOR THE TEASERS IMAGES
define('UPLOAD_PATH_TEASERS_IMAGES_VIEW', UPLOADS_URL.'teasers_images/');

//URL FOR THE TRAINING Video
define('UPLOAD_PATH_TRAINING_VIDEO',UPLOADS_PATH.'training_video/');
//define('ALLOWED_TYPES_TRAINING_VIDEO', 'mp4|3gp|mkv|avi');
define('ALLOWED_TYPES_TRAINING_VIDEO', 'mp4');
//URL FOR THE TRAINING Sounds
define('UPLOAD_PATH_TRAINING_SOUND',UPLOADS_PATH.'training_sounds/');
define('ALLOWED_TYPES_TRAINING_SOUND', 'mp3|ogg');



define('CDN_IMAGE_UPLOAD','0');
define('IMAGE_RESIZE','1');
define('Api_value', 'Odb1!Eg90_jIf*Ck#koXFV');
define('FOCUS_AREA_NOT_SELECTED_TEASER' , '79');
define('FOCUS_AREA_SELECTED_TEASER' , '92');
?>
