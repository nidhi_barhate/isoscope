<?php
class MY_Controller extends CI_Controller {
     
    public $data;
    function __construct()
    {
        parent::__construct();
        // Check Authentication
        if(isset($this->uri->rsegments[2])){
            $thisController = $this->uri->rsegments[2];
            $thisMainController= $this->uri->rsegments[1];
            if(in_array($thisController, $this->accessRules['@'])){
                // Admin side after login
                if($this->session->userdata('admin_loggedin') != 1)
                {
                    redirect(admin_url() . 'user/login');
                    exit;
                }
                $this->currentcontroller = $this->uri->rsegments[1];
                $this->currentmethod = $this->uri->rsegments[2];
            }elseif(in_array($thisController, $this->accessRules['$'])){
                // Front side after login
                if($this->session->userdata('front_loggedin') != 1)
                {
                    redirect(SITE_URL . 'user/login');
                    exit;
                }
            }elseif(in_array($thisController, $this->accessRules['#'])){
                // Webservice after login
                $this->section = 'webservice';
            }elseif(in_array($thisController, $this->accessRules['*'])){
                 // Login not required
                if(preg_match('/admin_/', $thisController))
                {
                    $this->section = 'admin';
                }
                else
                {
                    $this->section = 'front';    
                }     
            }else{
                //echo "Invalid URL";
                redirect(SITE_URL . 'user/login');
                exit;
            }
        }

    }
}
