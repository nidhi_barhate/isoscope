<?php

/**
 * /application/core/MY_Loader.php
 *
 */
class MY_Loader extends CI_Loader{
    public $add_css = array();
    public $add_js = array();
    
    function __construct() {
        parent::__construct();
    }

    //For FrontPage, After Loggin Pages And Admin page
    public function template($template_name,$vars = array(), $return = FALSE){
        $template_content = $this->view($template_name,$vars,true);
        $data['js'] = $this->add_js();
        $data['css'] = $this->add_css();
        $data['content'] = $template_content;

        $requestUri = $_SERVER['REQUEST_URI'];
        $content = "";
        $content = $this->view('templates/template',$data,$return);
        /*if (preg_match('/' . ADMIN_URL . '/', $requestUri)) {
             $content = $this->view('templates/template_admin',$data,$return);
        } else {
             $content = $this->view('templates/template',$data,$return);
        }*/

        if ($return) {
            return $content;
        }

    }

    public function add_js(){
            $js = $this->add_js;
            $js_str = '';
            foreach ($js as $item){
                $js_str .= '<script type="text/javascript" src="'.base_url().'assets/js/'.$item.'"></script>';
            }
            return $js_str;
    }
    
    public function add_css(){
           $css = $this->add_css;
           $css_str = '';
           foreach ($css as $item){
                $css_str .= '<link rel="stylesheet" href="'.base_url().'assets/css/'.$item.'" type="text/css" />';
           }
           return $css_str;
    }
}
