<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Email extends CI_Email {
 
    function send_mail($strMailContent, $arrOptions, $arrValues) {
        
        // Logic for replacing custom variables in email content
        
        $arrKeys = array_keys($arrValues);
        array_walk($arrKeys, function (&$value, $key) {
            $value = "[$value]";
        });
        $message = str_replace($arrKeys, $arrValues, $strMailContent);
            // Set Email Settings
           // global $config;
            //re($config);
          //  $this->initialize($config);

        $configuration['protocol'] = PROTOCOL; 
        $configuration['smtp_host'] = SMTP_HOST;
        $configuration['smtp_port'] = SMTP_PORT;
        $configuration['smtp_user'] = SMTP_USER;
        $configuration['smtp_pass'] = SMTP_PASS;
        $configuration['smtp_crypto'] = SMTP_CRYPTO;
        $configuration['mailtype'] = MAILTYPE;
        $this->initialize($configuration);
        $this->clear();
        $this->set_newline("\r\n");
        $this->smtp_crypto = SMTP_CRYPTO;
        $this->from(MAIL_FROMEMAIL, MAIL_FROMNAME);
        $this->subject($arrOptions['subject']);
        if (isset($arrOptions['to'])) {
            $this->to($arrOptions['to']);
        }
        if (isset($arrOptions['cc'])) {
            $this->cc($arrOptions['cc']);
        }
        if (isset($arrOptions['bcc'])) {
            $this->bcc($arrOptions['bcc']);
        }
        $this->message($message);
        
        if ($this->send()) {
            return 1;
        } else {
            return 0;
            //return show_error($this->print_debugger());
        }
    }

}

// END MY Email Class

/* End of file MY_Email.php */
/* Location: ./application/libraries/MY_Email.php */ 