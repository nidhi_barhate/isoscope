<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css");?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.css");?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/AdminLTE.min.css");?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/skins/_all-skins.min.css");?>">
  <!-- iCheck -->
  <!-- <link rel="stylesheet" href="<?php echo base_url("assets/plugins/iCheck/flat/blue.css");?>"> -->
    <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url("assets/plugins/iCheck/all.css");?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url("assets/plugins/morris/morris.css");?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url("assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css");?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url("assets/plugins/datepicker/datepicker3.css");?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url("assets/plugins/daterangepicker/daterangepicker.css");?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css");?>">

  <link rel="stylesheet" href="<?php echo base_url("assets/css/custom.css");?>">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

 <!--CSS will be add dynamically-->
 <?php echo $css;?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url("assets/dist/img/defaultUser.png");?>" class="user-image" alt="User Image">
              <span class="hidden-xs">Admin</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url("assets/dist/img/defaultUser.png");?>" class="img-circle" alt="User Image">

                <p>
                 <!--  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small> -->
                  Admin
                </p>
              </li>
              <!-- Menu Body -->
<!--               <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row --
              </li> -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <!-- <a href="#" class="btn btn-default btn-flat">Profile</a> -->
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url("admin/user/logout");?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url("assets/dist/img/defaultUser.png");?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php 
        $thisController = $this->uri->rsegments[1]; 
        if(isset($this->uri->rsegments[3])){
          $thisRole = $this->uri->rsegments[3];
        }
      ?>
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?php echo ($thisController == 'user'&& !isset($thisRole))?"active":""?>">
          <a href="<?php echo admin_url().'user/view'; ?>">
            <i class="fa fa-users"></i> <span>Users</span>
          </a>
        </li>
        <li class="<?php echo ($thisController == 'user' && isset($thisRole) && $thisRole == ROLE_PATHOLOGIST)?"active":""?>">
          <a href="<?php echo admin_url().'user/view/'.ROLE_PATHOLOGIST; ?>">
            <i class="fa fa-users"></i> <span>Pathologist</span>
          </a>
        </li>
        <li class="<?php echo ($thisController == 'role')?"active":""?>">
          <a href="<?php echo admin_url().'role/view'; ?>">
            <i class="fa fa-circle-o"></i> <span>Role</span>
          </a>
        </li>
        <li class="<?php echo ($thisController == 'campaign')?"active":""?>">
          <a href="<?php echo admin_url().'campaign/view'; ?>">
            <i class="fa fa-circle-o"></i> <span>Campaign</span>
          </a>
        </li>

        <li class="<?php echo ($thisController == 'patient')?"active":""?>">
          <a href="<?php echo admin_url().'patient/view'; ?>">
            <i class="fa fa-circle-o"></i> <span>Patient</span>
          </a>
        </li>

        <li class="<?php echo ($thisController == 'diseases')?"active":""?>">
          <a href="<?php echo admin_url().'diseases/view'; ?>">
            <i class="fa fa-circle-o"></i> <span>Diseases</span>
          </a>
        </li>
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Recharge</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> Prepaid/Postpaid</a></li>
            <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> DTH </a></li>
            <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Gas</a></li>
            <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Electricity</a></li>
          </ul>
        </li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper p20 hp100">
  <div class="box-body pb0">
            <?php 
            $errors = validation_errors();
            if(!empty($errors)) { ?>
            <div class="alert alert-danger alert-dismissable mb0">
                <!-- <i class="fa fa-ban"></i> -->
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&#215;</button>
                <?php echo validation_errors(); ?>                                   
            </div>
            <?php } ?>
            <?php
            if(isset($error_message) && $error_message != '') { ?>
            <div class="alert alert-danger alert-dismissable mb0">
                <!-- <i class="fa fa-ban"></i> -->
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&#215;</button>
                <?php echo $error_message; ?>                                   
            </div>
            <?php } ?>
            <?php $arrFlashError = $this->session->flashdata('error_message'); ?>
            <div class="alert alert-danger alert-dismissable mb0" style="display: <?php if(!empty($arrFlashError)) { ?>block<?php } else { ?>none<?php } ?>;">
                <!-- <i class="fa fa-ban"></i> -->
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&#215;</button>
                <div id="error_flash_message">
                    <?php 
                    if(!empty($arrFlashError)) {
                        echo ucfirst(implode('<br>', $this->session->flashdata('error_message')));
                    } 
                    ?>
                </div>
            </div>
            <?php $arrFlashSuccess = $this->session->flashdata('success_message'); ?>
            <div class="alert alert-success alert-dismissable mb0" style="display: <?php if(!empty($arrFlashSuccess)) { ?>block<?php }else { ?>none<?php } ?>;">
                <!-- <i class="fa fa-check"></i>-->
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&#215;</button>
                <div id="success_flash_message">
                    <?php 
                    if(!empty($arrFlashSuccess)) {
                        echo ucfirst(implode('<br>', $this->session->flashdata('success_message'))); 
                    }
                    ?>
                </div>
            </div>
        </div>
   <section class="content-header">
      <h1>
        <?php //echo (isset($page_title)?$page_title:"");?>
       <!--  <small>Control panel</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <?php 
        if(!empty($breadcrumbs) && isset($breadcrumbs)){
          foreach ($breadcrumbs as $key => $value) { ?>
              <li class="active"><?php echo $value['name']; ?></li>
        <?php  }
        }
        ?>
        
        
      </ol>
    </section>
    
  <!--Show Here Conent-->
            <?php echo $content;?>
    <!-- Content Header (Page header) -->
   
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.6
    </div>
    <strong>Copyright &copy; 2016 <a href="#">IsoScope</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->
<script type="text/javascript">
        var BASE_URL = '<?php echo base_url(); ?>';
        var SITE_URL = '<?php echo site_url(); ?>';
        var assetsURL = BASE_URL+"assets/";
        var ADMIN_URL = '<?php  echo admin_url(); ?>';
        var csrf_data = {name:'<?php echo $this->security->get_csrf_token_name(); ?>',value:'<?php echo ($this->security->get_csrf_hash() != NULL)?$this->security->get_csrf_hash():"0"; ?>'};
        var blockUIElement = '<div class="col-xs-12"><div id="loadingimg"><img src="<?php echo base_url("assets/images/ajax-loader1.gif");?>"/></div></div>';
    </script>
<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url("assets/plugins/datatables/jquery.dataTables.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.min.js"); ?>"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url("assets/plugins/morris/morris.min.js"); ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url("assets/plugins/sparkline/jquery.sparkline.min.js"); ?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url("assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"); ?>"></script>

<!-- jQuery BlockUI -->
<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.blockUI.js"); ?>"></script>

<!-- jQuery Knob Chart -->
<script src="<?php echo base_url("assets/plugins/knob/jquery.knob.js"); ?>"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url("assets/plugins/daterangepicker/daterangepicker.js"); ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url("assets/plugins/datepicker/bootstrap-datepicker.js"); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"); ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url("assets/plugins/slimScroll/jquery.slimscroll.min.js"); ?>"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url("assets/plugins/iCheck/icheck.min.js"); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("assets/plugins/fastclick/fastclick.js"); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("assets/dist/js/app.min.js"); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php //echo base_url("assets/dist/js/pages/dashboard.js"); ?>"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?php //echo base_url("assets/dist/js/demo.js"); ?>"></script> -->
<script src="<?php echo base_url("assets/js/common_function.js"); ?>"></script>
<!--JS will be add dynamically-->
<?php echo $js;?>
<script type="text/javascript"></script>
<script src="<?php echo base_url("assets/js/custom.js"); ?>"></script>
</body>
</html>