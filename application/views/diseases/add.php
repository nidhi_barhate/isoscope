<div class="row pt40">
    <div class="col-md-12">
        <div class="box p15">
           <div class="box-title">
               <h3><?php echo (isset($diseasesName) && $diseasesName!='')?"Edit Diseases":"Add Diseases"; ?></h3>
           </div>    
           <div class="box-content">
              <!--<form diseases="form" id="validation-form" class="form-horizontal stdform margin" method="post" name="profileadd" enctype="multipart/form-data" action="">-->
              <?php  echo form_open_multipart('', array('id' => 'validation-form', 'name' => 'profileadd','class'=>'form-horizontal stdform margin',"novalidate"=>"novalidate")); ?>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Diseases Name:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Diseases Name" name="diseasesName" id="diseasesName" value="<?php if(isset($diseasesName) && $diseasesName!=''){echo $diseasesName;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Diseases Short Code:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Diseases Short Code" name="diseasesShortCode" id="diseasesShortCode" value="<?php if(isset($diseasesShortCode) && $diseasesShortCode!=''){echo $diseasesShortCode;} ?>" data-rule-required="true" />                   
                      </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <button type="submit" name="eventSubmit" value="1" class="btn btn-primary">Submit</button>
                        <a href="<?php echo admin_url(); ?>diseases/view" class="color-black"><button type="button" class="btn" name="btncancel" value="">Cancel</button></a>
                    </div>
                </div>
              <!--</form>-->
              <?php echo form_close(); ?>
           </div>   
            
        </div>
    </div>
</div>