   <div class="row pt40">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <div class="box-title">
               <h3>Manage Diseases</h3>
           </div>   
          <div class="pull-right"><a href="<?php echo admin_url().'diseases/add'; ?>" class="btn btn-block btn-primary">Add Diseases</a></div>
        </div>
        <!-- /.box-header -->
        <div id="divResponse">
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>