<div class="row pt40">
    <div class="col-md-12">
        <div class="box p15">
           <div class="box-title">
               <h3><?php echo (isset($campaignName) && $campaignName!='')?"Edit Campaign":"Add Campaign"; ?></h3>
           </div>    
           <div class="box-content">
              <!--<form campaign="form" id="validation-form" class="form-horizontal stdform margin" method="post" name="profileadd" enctype="multipart/form-data" action="">-->
              <?php  echo form_open_multipart('', array('id' => 'validation-form', 'name' => 'profileadd','class'=>'form-horizontal stdform margin',"novalidate"=>"novalidate")); ?>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Campaign Name:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Campaign Name" name="campaignName" id="campaignName" value="<?php if(isset($campaignName) && $campaignName!=''){echo $campaignName;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Location:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <textarea class="form-control" placeholder="Location" name="location" id="location" data-rule-required="true"><?php if(isset($location) && $location!=''){echo $location;} ?></textarea>                       
                      </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <button type="submit" name="eventSubmit" value="1" class="btn btn-primary">Submit</button>
                        <a href="<?php echo admin_url(); ?>campaign/view" class="color-black"><button type="button" class="btn" name="btncancel" value="">Cancel</button></a>
                    </div>
                </div>
              <!--</form>-->
              <?php echo form_close(); ?>
           </div>   
            
        </div>
    </div>
</div>