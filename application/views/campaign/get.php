<div class="box-body">
  <table id="commonDataTable" class="table table-bordered table-hover">
    <thead>
    <tr>
      <th></th>
      <th>Campaign Title</th>
      <th>Location</th>
      <th>Created Date</th>
      <th>Modified Date</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
    
    <?php 
    if(count($records) > 0){
      $i=1;
      foreach($records as $key=>$value){?>
        <tr>
          <td><?php echo $i; ?></td>
          <td><?php echo $value->campaignName; ?></td>
          <td><?php echo $value->location; ?></td>
          <td><?php echo ($value->createdDate != '0000-00-00 00:00:00')?date(DATE_FORMAT, strtotime($value->createdDate)):""; ?></td>
          <td><?php echo ($value->modifiedDate != '0000-00-00 00:00:00')?date(DATE_FORMAT, strtotime($value->modifiedDate)):""; ?></td>
          <td><a href="<?php echo admin_url().'campaign/add/'.$value->campaignId; ?>" class="btn btn-info"><i class="fa fa-fw fa-edit"></i></a> <a href="javascript:void(0);"  class="btn btn-danger deleteConfirm" data-moduleUrl="<?php echo admin_url().'campaign/delete/'.$value->campaignId; ?>"><i class="fa fa-fw fa-trash"></i></a></td>
        </tr>
    <?php ++$i;}
    }
    ?>
    </tbody>
    <tfoot>
    <tr>
      <th></th>
      <th>Campaign Title</th>
      <th>Location</th>
      <th>Created Date</th>
      <th>Modified Date</th>
      <th>Action</th>

    </tr>
    </tfoot>
  </table>
</div>
<!-- /.box-body -->
<script type="text/javascript">
  callDatatable();
</script>