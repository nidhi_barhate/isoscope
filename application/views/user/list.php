   <div class="row pt40">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <div class="box-title">
               <?php 
                if($roleId == ROLE_PATHOLOGIST){?>
                <h3>Manage Pathologist</h3>
               <?php }else{ ?>
                <h3>Manage User</h3>
               <?php }
               ?>
           </div>   
          <div class="pull-right"><?php 
                if($roleId == ROLE_PATHOLOGIST){?>
                <a href="<?php echo admin_url().'user/add/'.$roleId; ?>" class="btn btn-block btn-primary">Add Pathologist
               <?php }else{ ?>
                <a href="<?php echo admin_url().'user/add'; ?>" class="btn btn-block btn-primary">Add User
               <?php }
               ?></a></div>
        </div>
        <!-- /.box-header -->
        <div id="divResponse">
        </div>
        <input type="hidden" name="roleId" id="roleId" value="<?php echo $roleId; ?>">
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>