<?php //re($content);?>

<!-- banner start --> 
  <!-- ================ -->
  <div class="banner"> 
    <!-- slider revolution start --> 
    <!-- ================ -->
    <div class="slider-banner-container">
      <div class="slider-banner">
      <div class="banner-img"> <img src="<?php echo asset_url(); ?>img/home-banner.png"  alt="slidebg1"> <span class="banner-tagline">TRAINIERE, WERDE BESSER, MESSE DICH MIT DEINEN FREUNDEN! Trainingsplatz war gestern - Du trainierst überall. </span></div> 
          <div class="banner-app-img"><a href=""><img src="<?php echo asset_url(); ?>img/banner-app-store.png"  alt="slidebg1"></a></div> </div>
    </div>
    <!-- slider revolution end --> 
  </div>
  <!-- banner end --> 
  <!-- section start --> 
  <!-- ================ -->
  <div class="banner-bottom clearfix">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12 scrollme animateme"
				data-when="enter"
				data-from="0.5"
				data-to="0"
				data-crop="false"
				data-opacity="0"
				data-scale="1.5">
          <div class="row">
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
              <p><?php
echo base64_decode($content['TR_WEB_HOME_BLACK_LEFT_TEXT']);

?> </p>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
              <p><?php echo base64_decode($content['TR_WEB_HOME_BLACK_RIGHT_TEXT']); ?></p>
            </div>
            <div class="banner-logo-container"> <img src="<?php echo asset_url(); ?>img/banner-top-logo.png" /></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- section end --> 
  <!-- main-container start --> 
  <!-- ================ -->
  <section class="main-container section1 gray-bg"> 
    
    <!-- main start --> 
    <!-- ================ -->
    <div class="main">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12">
            <h1 class="text-center title"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_CENTER_TITLE']); ?></h1>
            <div class="separator"></div>
            <p class="text-center tag-line"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_CENTER_TEXT']); ?></p>
            <div class="row section-container">
              <div class="col-lg-4 col-sm-4 col-md-4 ">
                <div class="box-style-1 white-bg bg-green scrollme">
                <div class="animateme no-space" data-when="view"
								data-from="0.5"
								data-to="0"
								data-opacity="0">
                  <h2 class="mt-55 title"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_CENTER_BOX1_TITLE']); ?></h2>
                  <p class="desc"> <?php echo base64_decode($content['TR_WEB_HOME_GRAY_CENTER_BOX1_TEXT']); ?></p></div>
                </div>
              </div>
              <div class="col-lg-4 col-sm-4 col-md-4 ">
                <div class="box-style-1 white-bg bg-blue scrollme">
                	<div class="animateme no-space" data-when="view"
								data-from="0.5"
								data-to="0"
								data-opacity="0">
                  <h2 class="mt-55 title"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_CENTER_BOX2_TITLE']); ?></h2>
                  <p class="desc"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_CENTER_BOX2_TEXT']); ?></p></div>
                </div>
              </div>
              <div class="col-lg-4 col-sm-4 col-md-4 ">
                <div class="box-style-1 white-bg  bg-yellow scrollme">
                <div class="animateme no-space" data-when="view"
								data-from="0.5"
								data-to="0"
								data-opacity="0">
                  <h2 class="mt-55 title"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_CENTER_BOX3_TITLE']); ?></h2>
                  <p class="desc"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_CENTER_BOX3_TEXT']); ?></p></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-md-12 app-btn">
                <div class="col-lg-4 col-sm-4 col-md-4 col-md-push-4 col-sm-push-4 col-lg-push-4"> <a href="#" class="btn btn-dark btn-sm ">Jetzt APp laden!</a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- main end --> 
    
  </section>
  <!-- main-container end --> 
  
  <!-- main-container start --> 
  <!-- ================ -->
  <section class="main-container bg-purple section2"> 
    
    <!-- main start --> 
    <!-- ================ -->
    <div class="main">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12">
            <h1 class="title text-center"><?php echo base64_decode($content['TR_WEB_HOME_BLUE_CENTER_TITLE']); ?></h1>
            <p class="desc"><?php echo base64_decode($content['TR_WEB_HOME_BLUE_CENTER_TEXT']); ?></p>
          </div>
        </div>
      </div>
    </div>
    <!-- main end --> 
    
  </section>
  <!-- main-container end --> 
  <!-- demo start --> 
  <!-- ================ -->
  <div id="demo" class="clearfix">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12">
          <div class="embed-responsive"> 
            <!--<iframe class="embed-responsive-item" src="http://player.vimeo.com/video/29198414?byline=0&amp;portrait=0"></iframe>--> 
            <!--<img src="images/video.png">-->
            <iframe width="560" src="https://www.youtube.com/embed/slTNf0sweD8" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- demo end --> 
  <!-- main start --> 
  <!-- ================ -->
  <section class="main-container section1 gray-bg black-box"> 
    
    <!-- main start --> 
    <!-- ================ -->
    <div class="main">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12">
            <h1 class="text-center title mb-75"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_BOTTOM_TITLE']); ?></h1>
            <div class="separator"></div>
            <div class="row section-container">
              <div class="col-lg-4 col-sm-4 col-md-4 ">
                <div class="box-style-1 white-bg scrollme">
                <div class="animateme" data-when="view"
								data-from="0.5"
								data-to="0"
								data-opacity="0">
                  <h2 class="mt-55 title"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_BOTTOM_BOX1_TITLE']); ?></h2>
                  <p class="desc"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_BOTTOM_BOX1_TEXT']); ?></p></div>
                </div>
              </div>
              <div class="col-lg-4 col-sm-4 col-md-4 ">
                <div class="box-style-1 white-bg scrollme">
                <div class="animateme" data-when="view"
								data-from="0.5"
								data-to="0"
								data-opacity="0">
                  <h2 class="mt-55 title"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_BOTTOM_BOX2_TITLE']); ?></h2>
                  <p class="desc"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_BOTTOM_BOX2_TEXT']); ?></p></div>
                </div>
              </div>
              <div class="col-lg-4 col-sm-4 col-md-4 ">
                <div class="box-style-1 white-bg scrollme">
                <div class="animateme" data-when="view"
								data-from="0.5"
								data-to="0"
								data-opacity="0">
                  <h2 class="mt-55 title"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_BOTTOM_BOX3_TITLE']); ?></h2>
                  <p class="desc"><?php echo base64_decode($content['TR_WEB_HOME_GRAY_BOTTOM_BOX3_TEXT']); ?></p></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-md-12 app-btn">
                <div class="col-lg-4 col-sm-4 col-md-4 col-md-push-4 col-sm-push-4 col-lg-push-4 mb-120"> <a href="#" class="btn btn-dark btn-sm">Jetzt APp laden!</a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- main end --> 
    
  </section>
  <!-- main end -->
  
  </section>
