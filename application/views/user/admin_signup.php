<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Activity Genius Admin | Sign Up</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link href="<?php echo asset_url(); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link href="<?php echo asset_url(); ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition register-page">
    <div class="register-box">
      <div class="register-logo">
        <a href=""><b>Activity</b> Genius Signup</a>
      </div>

      <div class="register-box-body">
         <!------------------------------------------------------------------------>
        <?php 
                $errors = validation_errors();
                if(!empty($errors)) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&#215;</button>
                    <?php echo validation_errors(); ?>                                   
                </div>
                <?php } ?>
                <?php
                if(isset($error_message) && $error_message != '') { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&#215;</button>
                    <?php echo $error_message; ?>                                   
                </div>
                <?php } ?>
                <?php $arrFlashError = $this->session->flashdata('error_message'); ?>
                <div class="alert alert-danger alert-dismissable" style="display: <?php if(!empty($arrFlashError)) { ?>block<?php } else { ?>none<?php } ?>;">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&#215;</button>
                    <div id="error_flash_message">
                        <?php 
                        if(!empty($arrFlashError)) {
                            echo ucfirst(implode('<br>', $this->session->flashdata('error_message')));
                        } 
                        ?>
                    </div>
                </div>
                <?php $arrFlashSuccess = $this->session->flashdata('success_message'); ?>
                <div class="alert alert-success alert-dismissable" style="display: <?php if(!empty($arrFlashSuccess)) { ?>block<?php }else { ?>none<?php } ?>;">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&#215;</button>
                    <div id="success_flash_message">
                        <?php 
                        if(!empty($arrFlashSuccess)) {
                            echo ucfirst(implode('<br>', $this->session->flashdata('success_message'))); 
                        }
                        ?>
                    </div>
                </div>
            <!------------------------------------------------------------------------>
        <p class="login-box-msg">Register a new membership</p>
        <form action="" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Full name">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Retype password">
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox" class="iCheck"> I agree to the <a href="#">terms</a>
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
            </div><!-- /.col -->
          </div>
        </form>

        <!--<div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a>
        </div>-->

        <a href="<?php echo admin_url(); ?>user/login" class="text-center">I already have a membership</a>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->

       <!-- jQuery 2.1.4 -->
    <script src="<?php echo asset_url(); ?>js/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
     <script src="<?php echo asset_url() ; ?>js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?php echo asset_url() ; ?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
