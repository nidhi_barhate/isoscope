<?php
/**
 * @author lolkittens
 * @copyright 2015
 */
 //re($plan_list);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="col-xs-12">
                    <h3 class="box-title">Add User</h3>
                </div>
            </div>
             <form role="form" id="form1" class="stdform margin" method="post" name="profileadd" action="">
                    	<div class="box-body">
                    	 <?php if($userId == ''){?>
                         <div class="form-group">
                        	<label>User Name</label>
                            <input type="text" class="form-control" placeholder="User Name" name="userName" id="userName" class="longinput" value="<?php echo $userName;?>" />                         
                       </div>
                        <?php } ?>
                        <div class="form-group">
                        	<label>Email Id</label>
                            <span class="field">
                            <input type="text" class="form-control" placeholder="Email Id" name="emailId" id="emailId" class="longinput" value="<?php echo $emailId; ?>" />                         
                          </div>
                          <div class="form-group">
                        	<label>First Name</label>
                            <input type="text" class="form-control" placeholder="First Name" name="firstName" id="firstName" class="longinput" value="<?php echo $firstName; ?>" />                         
                          </div>
                          <div class="form-group">
                        	<label>Last Name</label>
                            <input type="text" class="form-control" placeholder="Last Name" name="lastName" id="lastName" class="longinput" value="<?php echo $lastName; ?>" />                         
                          </div>
                          <?php if($userId == ''){?>
                            <div class="form-group">
                        	<label>Password</label>
                            <input type="password" class="form-control" placeholder="Password" name="password" id="password" class="longinput" value="<?php echo $password; ?>" />                         
                          </div>
                          <?php } ?>
                          <div class="form-group">
                        	<label>Organization Name</label>
                            <input type="text" class="form-control" placeholder="Organization Name" name="organizationName" id="organizationName" class="longinput" value="<?php echo $organizationName; ?>" />  
                          </div>
                           <div class="form-group">
                                            <label>Plan</label>
                                            <select name="planId" class="uniformselect form-control" >
                                                <option value="">Choose Plan</option>
                                                <?php foreach($plan_list as $val) { ?>
                                                <option value="<?php echo $val->planId; ?>"  <?php if(isset($planId)){ if($planId == $val->planId ){ echo 'selected="selected"'; } }  ?> ><?php echo $val->planName; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div><!-- /.box-body -->
                              <div class="box-footer">
                                        <button type="submit" name="btnsubmit" value="1" class="btn btn-primary">Submit</button>
                                        <a href="<?php echo user_url(); ?>view"><button type="button" class="btn btn-primary" name="btncancel" value="">Cancel</button></a>
                                    </div>
                    </form>
		</div>
    </div>
</div>