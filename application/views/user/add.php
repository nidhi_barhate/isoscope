<div class="row pt40">
    <div class="col-md-12">
        <div class="box p15">
           <div class="box-title">
               <h3><?php echo (isset($userName) && $userName!='')?"Edit User":"Add User"; ?></h3>
           </div>    
           <div class="box-content">
              <!--<form user="form" id="validation-form" class="form-horizontal stdform margin" method="post" name="profileadd" enctype="multipart/form-data" action="">-->
              <?php  echo form_open_multipart('', array('id' => 'validation-form', 'name' => 'profileadd','class'=>'form-horizontal stdform margin',"novalidate"=>"novalidate")); ?>
              
                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Role Name:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <select name="roleId" class="form-control" data-rule-required="true">
                              <?php 
                                if(count($role_list) > 0){
                                  foreach ($role_list as $key => $value) { ?>
                                     <option value="<?php echo $value->roleId; ?>" <?php echo ($value->roleId == $roleId)?"selected":""; ?>><?php echo $value->roleTitle; ?></option> 
                              <?php }
                                }
                              ?>
                          </select>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">User Name:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="User Name" name="userName" id="userName" value="<?php if(isset($userName) && $userName!=''){echo $userName;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>
                <?php if(isset($userName) && $userName==''){ ?>
                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Password:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="password" class="form-control" placeholder="Password" name="password" id="password" value="<?php if(isset($password) && $password!=''){echo $password;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>
                <?php }?>
                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Email:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Email" name="email" id="email" value="<?php if(isset($email) && $email!=''){echo $email;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">First Name:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="First Name" name="firstName" id="firstName" value="<?php if(isset($firstName) && $firstName!=''){echo $firstName;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Last Name:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Last Name" name="lastName" id="lastName" value="<?php if(isset($lastName) && $lastName!=''){echo $lastName;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>

              <!--   <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Profile Picture:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="file" class="form-control" name="profilePic" id="profilePic" data-rule-required="true"/> 
                          <input type="hidden" name="profilePic" id="profilePic" value="<?php echo $profilePic;?>"/>                        
                      </div>
                </div> -->

                
                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Gender:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                           <input type="radio" class="minimal"  name="gender" data-rule-required="true" value="1" <?php echo ($gender == 1)?"checked":""; ?>> Male
                           <input type="radio" class="minimal"  name="gender" data-rule-required="true" value="2" <?php echo ($gender == 2)?"checked":""; ?>> Female
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Date Of Birth:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Date Of Birth" name="dateOfBirth" id="dateOfBirth" value="<?php echo ($dateOfBirth != '0000-00-00')?date(DATE_FORMAT, strtotime($dateOfBirth)):""; ?>" data-rule-required="true" readonly/>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Phone No:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Phone No" name="phoneNo" id="phoneNo" value="<?php if(isset($phoneNo) && $phoneNo!=''){echo $phoneNo;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <button type="submit" name="eventSubmit" value="1" class="btn btn-primary">Submit</button>
                        <a href="<?php echo admin_url(); ?>user/view" class="color-black"><button type="button" class="btn" name="btncancel" value="">Cancel</button></a>
                    </div>
                </div>
              <!--</form>-->
              <?php echo form_close(); ?>
           </div>   
            
        </div>
    </div>
</div>