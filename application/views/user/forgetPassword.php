<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Activity Genius Admin | Forgot Password</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link href="<?php echo asset_url(); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link href="<?php echo asset_url(); ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo asset_url(); ?>css/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href=""><b>Activity</b> Genius Admin</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <!------------------------------------------------------------------------>
        <?php 
                $errors = validation_errors();
                if(!empty($errors)) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&#215;</button>
                    <?php echo validation_errors(); ?>                                   
                </div>
                <?php } ?>
                <?php
                if(isset($error_message) && $error_message != '') { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&#215;</button>
                    <?php echo $error_message; ?>                                   
                </div>
                <?php } ?>
                <?php $arrFlashError = $this->session->flashdata('error_message'); ?>
                <div class="alert alert-danger alert-dismissable" style="display: <?php if(!empty($arrFlashError)) { ?>block<?php } else { ?>none<?php } ?>;">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&#215;</button>
                    <div id="error_flash_message">
                        <?php 
                        if(!empty($arrFlashError)) {
                            echo ucfirst(implode('<br>', $this->session->flashdata('error_message')));
                        } 
                        ?>
                    </div>
                </div>
                <?php $arrFlashSuccess = $this->session->flashdata('success_message'); ?>
                <div class="alert alert-success alert-dismissable" style="display: <?php if(!empty($arrFlashSuccess)) { ?>block<?php }else { ?>none<?php } ?>;">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&#215;</button>
                    <div id="success_flash_message">
                        <?php 
                        if(!empty($arrFlashSuccess)) {
                            echo ucfirst(implode('<br>', $this->session->flashdata('success_message'))); 
                        }
                        ?>
                    </div>
                </div>
            <!------------------------------------------------------------------------>
        <p class="login-box-msg">Forget Password<br/>Enter your registerrd email or username</p>
        <form action="" method="post">
          <div class="form-group has-feedback">
                <input type="text" name="emailoruserName" class="form-control" placeholder="Email Or User Name"/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          
          <div class="row">
            <div class="col-xs-6">
              <button type="submit" name="btnSubmit" value="1" class="btn btn-primary btn-block btn-flat">Ok</button>
              </div>
              <div class="col-xs-6">
              <a href="<?php echo admin_url().'user/login';?>" name="btnSubmit" class="btn btn-primary btn-block btn-flat">Cancel</a>
            </div><!-- /.col -->
          </div>
        </form>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo asset_url(); ?>js/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
     <script src="<?php echo asset_url() ; ?>js/bootstrap.min.js" type="text/javascript"></script>
  </body>
</html>