<div class="row pt40">
    <div class="col-md-12">
        <div class="box p15">
           <div class="box-title">
               <h3><?php echo (isset($userName) && $userName!='')?"Edit Patient":"Add Patient"; ?></h3>
           </div>    
           <div class="box-content">
              <!--<form user="form" id="validation-form" class="form-horizontal stdform margin" method="post" name="profileadd" enctype="multipart/form-data" action="">-->
              <?php  echo form_open_multipart('', array('id' => 'validation-form', 'name' => 'profileadd','class'=>'form-horizontal stdform margin',"novalidate"=>"novalidate")); ?>
              
                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Campaign Name:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <select name="campaignId" class="form-control" data-rule-required="true">
                              <?php 
                                if(count($campaign_list) > 0){
                                  foreach ($campaign_list as $key => $value) { ?>
                                     <option value="<?php echo $value->campaignId; ?>" <?php echo ($value->campaignId == $campaignId)?"selected":""; ?>><?php echo $value->campaignName; ?></option> 
                              <?php }
                                }
                              ?>
                          </select>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">First Name:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="First Name" name="firstName" id="firstName" value="<?php if(isset($firstName) && $firstName!=''){echo $firstName;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Last Name:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Last Name" name="lastName" id="lastName" value="<?php if(isset($lastName) && $lastName!=''){echo $lastName;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Race:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                           <select class="form-control" name="race">
                             <option>--No Select--</option>
                             <option value="1" <?php echo ($race == 1)?"selected":""; ?>>Black</option>
                             <option value="2" <?php echo ($race == 2)?"selected":""; ?>>White</option>
                             <option value="3" <?php echo ($race == 3)?"selected":""; ?>>Coloured</option>
                             <option value="4" <?php echo ($race == 4)?"selected":""; ?>>Indian</option>
                             <option value="5" <?php echo ($race == 5)?"selected":""; ?>>Other</option>
                           </select>
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Gender:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                           <input type="radio" class="minimal"  name="gender" data-rule-required="true" value="1" <?php echo ($gender == 1)?"checked":""; ?>> Male
                           <input type="radio" class="minimal"  name="gender" data-rule-required="true" value="2" <?php echo ($gender == 2)?"checked":""; ?>> Female
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Date Of Birth:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Date Of Birth" name="dateOfBirth" id="dateOfBirth" value="<?php if(isset($dateOfBirth) && $dateOfBirth!=''){echo $dateOfBirth;} ?>" data-rule-required="true" readonly/>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Profile Picture:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="file" class="form-control" name="profilePic" id="profilePic" data-rule-required="true"/> 
                          <input type="hidden" name="profilePichidden" id="profilePic" value="<?php echo $profilePic;?>"/>                        
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Id Number:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Id Number" name="idNumber" id="idNumber" value="<?php if(isset($idNumber) && $idNumber!=''){echo $idNumber;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Cell No:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Cell No" name="phoneNo" id="phoneNo" value="<?php if(isset($phoneNo) && $phoneNo!=''){echo $phoneNo;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Disability:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                            <select class="form-control" name="disability">
                             <option>--No Select--</option>
                             <option value="1" <?php echo ($disability == 1)?"selected":""; ?>>Yes</option>
                             <option value="2" <?php echo ($disability == 2)?"selected":""; ?>>No</option>
                           </select>
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Nationality:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                            <select class="form-control" name="nationality">
                             <option>--No Select--</option>
                             <option value="1" <?php echo ($nationality == 1)?"selected":""; ?>>South African</option>
                             <option value="2" <?php echo ($nationality == 2)?"selected":""; ?>>Other</option>
                           </select>
                      </div>
                </div>
          
               <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Family member’s Name:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Family member’s Name" name="familyMemberName" id="familyMemberName" value="<?php if(isset($familyMemberName) && $familyMemberName!=''){echo $familyMemberName;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Family member’s No:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <input type="text" class="form-control" placeholder="Family member’s No" name="familyMemeberPhoneNo" id="familyMemeberPhoneNo" value="<?php if(isset($familyMemeberPhoneNo) && $familyMemeberPhoneNo!=''){echo $familyMemeberPhoneNo;} ?>" data-rule-required="true"/>                         
                      </div>
                </div>
              
                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Patient’s Medical history:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <textarea class="form-control" name="patientMedicalHistory" id="patientMedicalHistory" data-rule-required="true"><?php if(isset($patientMedicalHistory) && $patientMedicalHistory!=''){echo $patientMedicalHistory;} ?></textarea>
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Allergies:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <textarea class="form-control" name="allergies" id="allergies" data-rule-required="true"><?php if(isset($allergies) && $allergies!=''){echo $allergies;} ?></textarea>
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Nearest health Institution:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <textarea class="form-control" name="nearestHealthInstitution" id="nearestHealthInstitution" data-rule-required="true"><?php if(isset($nearestHealthInstitution) && $nearestHealthInstitution!=''){echo $nearestHealthInstitution;} ?></textarea>
                      </div>
                </div>

                <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label" rule="required">Location:</label>
                      <div class="col-sm-6 col-lg-4 controls">
                          <textarea class="form-control" name="location" id="location" data-rule-required="true"><?php if(isset($location) && $location!=''){echo $location;} ?></textarea>
                      </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <button type="submit" name="eventSubmit" value="1" class="btn btn-primary">Submit</button>
                        <a href="<?php echo admin_url(); ?>patient/view" class="color-black"><button type="button" class="btn" name="btncancel" value="">Cancel</button></a>
                    </div>
                </div>
              <!--</form>-->
              <?php echo form_close(); ?>
           </div>   
            
        </div>
    </div>
</div>