<div class="box-body">
  <table id="commonDataTable" class="table table-bordered table-hover">
    <thead>
    <tr>
      <th></th>
      <td>Image</td>
      <th>Name</th>
      <th>Gender</th>
      <th>Id Number</th>
      <th>Date Of Birth</th>
      <th>Disability</th>
      <th>Created Date</th>
      <th>Modified Date</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
    
    <?php 
    if(count($records) > 0){
      $i=1;
      foreach($records as $key=>$value){?>
        <tr>
          <td><?php echo $i; ?></td>
          <td><?php if($value->profilePic != '') {?><img src="<?php echo UPLOAD_IMAGE_URL.$value->profilePic; ?>" height="80" width="auto" /><?php }?></td>
          <td><?php echo $value->firstName.' '.$value->lastName; ?></td>
          <td><?php echo ($value->gender == 1)?"M":"F"; ?></td>
          <td><?php echo $value->idNumber; ?></td>
          <td><?php echo ($value->dateOfBirth != '0000-00-00')?date(DATE_FORMAT, strtotime($value->dateOfBirth)):""; ?></td>
          <td><?php echo ($value->disability == 1)?"Yes":"No"; ?></td>
          <td><?php echo ($value->createdDate != '0000-00-00 00:00:00')?date(DATE_FORMAT, strtotime($value->createdDate)):""; ?></td>
          <td><?php echo ($value->modifiedDate != '0000-00-00 00:00:00')?date(DATE_FORMAT, strtotime($value->modifiedDate)):""; ?></td>
          <td><a href="<?php echo admin_url().'patient/add/'.$value->patientId; ?>" class="btn btn-info"><i class="fa fa-fw fa-edit"></i></a> <a href="javascript:void(0);"  class="btn btn-danger deleteConfirm" data-moduleUrl="<?php echo admin_url().'patient/delete/'.$value->patientId; ?>"><i class="fa fa-fw fa-trash"></i></a></td>
        </tr>
    <?php ++$i;}
    }
    ?>
    </tbody>
    <tfoot>
    <tr>
      <th></th>
      <th>Image</th>
      <th>Name</th>
      <th>Gender</th>
      <th>Id Number</th>
      <th>Date Of Birth</th>
      <th>Disability</th>
      <th>Created Date</th>
      <th>Modified Date</th>
      <th>Action</th>  </tr>
    </tfoot>
  </table>
</div>
<!-- /.box-body -->
<script type="text/javascript">
  callDatatable();
</script>