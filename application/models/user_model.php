<?php

class User_model extends MY_Model
{

    /****************************************/
     public $_table = 'users';
     public $primary_key = 'userId';
     protected $soft_delete = TRUE;
     protected $soft_delete_key = 'isDeleted';
    /****************************************/
	

	/*
	* Check Login
	*/
	function check_admin_login()
	{
        $this->db->select('U.userId,U.lastLoginDate,U.userName,U.firstName,U.lastName,U.pinnumber,,U.email,U.phoneNo,U.password,R.roleId,R.roleName,U.profilePic');
        if(isset($this->userOrEmail) && $this->userOrEmail != ''){
			$where = "(U.email = '".$this->userOrEmail."' OR U.userName = '".$this->userOrEmail."') ";
			$this->db->where($where);
		}
		$where2 = "(R.roleId = '".ROLE_ADMIN."' OR R.roleId = '".ROLE_PATHOLOGIST."') ";
		$this->db->where($where2);
		$this->db->where('U.isDeleted',0);
		$this->db->where('U.password',$this->password);
		$this->db->from('users AS U');
		$this->db->join('roles R','R.roleId = U.roleId');
		$query = $this->db->get();
        return $query->result();
	}

	function get_user_records($roleId=''){
		$this->db->select('U.*,R.roleTitle');
		if($roleId != ''){
			$this->db->where('R.roleId =',ROLE_PATHOLOGIST);
		}else{
			$this->db->where('R.roleId !=',ROLE_PATIENT);
			$this->db->where('R.roleId !=',ROLE_PATHOLOGIST);
		}
		$this->db->where('U.isDeleted',0);
		$this->db->from('users AS U');
		$this->db->join('roles AS R', 'U.roleId = R.roleId');
		$query = $this->db->get();
		return $query->result();
	}

	function check_userName($userName){
		$this->db->select('U.userId,U.userName');
		$this->db->where('U.isDeleted',0);
		$this->db->where('U.userName',$userName);
		$this->db->from('users AS U');
		$query = $this->db->get();
		$result = $query->result();
		if(!empty($result)) return 1;
		else return 0;
	}

	function check_email($email){
		$this->db->select('U.userId,U.email');
		$this->db->where('U.isDeleted',0);
		$this->db->where('U.email',$email);
		$this->db->from('users AS U');
		$query = $this->db->get();
		$result = $query->result();
		if(!empty($result)) return 1;
		else return 0;
	}

	function get_user_records_by_email($email){
		$this->db->select('U.*');
		$this->db->where('U.isDeleted',0);
		$this->db->where('U.email',$email);
		//$this->db->where('U.roleId !=',ROLE_PATHOLOGIST);
		$this->db->where('U.isDeleted',0);
		$this->db->from('users AS U');
		$query = $this->db->get();
		return $query->result();
	}


}
?>
