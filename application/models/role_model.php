<?php

class Role_model extends MY_Model{
    /****************************************/
     public $_table = 'roles';
     public $primary_key = 'roleId';
     protected $soft_delete = TRUE;
     protected $soft_delete_key = 'isDeleted';
    /****************************************/
}

?>