<?php

class Campaign_model extends MY_Model{
    /****************************************/
     public $_table = 'campaign';
     public $primary_key = 'campaignId';
     protected $soft_delete = TRUE;
     protected $soft_delete_key = 'isDeleted';
    /****************************************/

    function campaign_search($search,$pageIndex,$pageSize){
		$total_data = $this->campaign_model->get_all();
		$total_records = count($total_data);
	
		$this->db->select('C.campaignId,C.campaignName,C.createdDate as date,C.location');
		if($search != ''){
			$this->db->like('C.campaignName',$search);
		}
		
		if($pageIndex != '' && $pageSize != ''){
			if($total_records > ($pageSize * $pageIndex)){
				$offset = (($pageSize * $pageIndex) - $pageSize);
				$limit = ($pageSize * $pageIndex) - $offset;
				$this->db->limit($limit,$offset);
			} 
			else{
				$offset = (($pageSize * $pageIndex) - $pageSize); 
				$limit = $total_records - $offset;
				$this->db->limit($limit,$offset);
			} 
		}
		$this->db->where('C.isDeleted',0);
		$this->db->from('campaign AS C');
		$query = $this->db->get();
		//echo $this->db->last_query()."<br/><br/>";
		return $query->result();

	}
}

?>