<?php

class Patient_model extends MY_Model
{

    /****************************************/
     public $_table = 'patient';
     public $primary_key = 'patientId';
/*     protected $soft_delete = TRUE;
     protected $soft_delete_key = 'isDeleted';*/
    /****************************************/

	function get_patient_records(){
		$this->db->select('U.*,P.*,C.campaignName');
		$this->db->where('U.roleId',ROLE_PATIENT);
		$this->db->where('U.isDeleted',0);
		$this->db->from('patient AS P');
		$this->db->join('users AS U', 'U.userId = P.patientId');
		$this->db->join('campaign AS C', 'C.campaignId = P.campaignId');
		$query = $this->db->get();
		return $query->result();
	} 

	function get_patient_record_by_id($id){
		$this->db->select('U.*,P.*,C.campaignName');
		$this->db->where('P.patientId',$id);
		$this->db->where('U.roleId',ROLE_PATIENT);
		$this->db->from('patient AS P');
		$this->db->join('users AS U', 'U.userId = P.patientId');
		$this->db->join('campaign AS C', 'C.campaignId = P.campaignId');
		$query = $this->db->get();
		return $query->result();
	}

	/*
	if(total_records > (pageSize * pageIndex)) ((pageSize * pageIndex) - pageSize)+1 to (pageSize * pageIndex)
	else ((pageSize * pageIndex) - pageSize)+1 to total_records
	*/
	function patient_search($search,$pageIndex,$pageSize,$campaignId){
		$total_data = $this->get_patient_records();
		$total_records = count($total_data);
	
		$this->db->select('U.*,P.*,C.campaignName');
		if($search != ''){
			$this->db->like('U.firstName',$search);
			$this->db->or_like('U.lastName',$search);
			$this->db->or_like('CONCAT(U.firstName," ",U.lastName)',$search);
		}
		
		if($pageIndex != '' && $pageSize != ''){
			if($total_records > ($pageSize * $pageIndex)){
				$offset = (($pageSize * $pageIndex) - $pageSize);
				$limit = ($pageSize * $pageIndex) - $offset;
				$this->db->limit($limit,$offset);
			} 
			else{
				$offset = (($pageSize * $pageIndex) - $pageSize); 
				$limit = $total_records - $offset;
				$this->db->limit($limit,$offset);
			} 
		}

		if($campaignId != '' || $campaignId != 0){
			$this->db->where('C.campaignId',$campaignId);
		}

		$this->db->where('U.roleId',ROLE_PATIENT);
		$this->db->from('patient AS P');
		$this->db->join('users AS U', 'U.userId = P.patientId AND U.isDeleted=0');
		$this->db->join('campaign AS C', 'C.campaignId = P.campaignId');
		$query = $this->db->get();
		//echo $this->db->last_query()."<br/><br/>";
		return $query->result();

	}
}
?>
