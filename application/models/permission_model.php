<?php

/**
* @author lolkittens
* @copyright 2015
*/
class permission_model extends MY_Model
{
    /****************************************/
     public $_table = 'permission';
     public $primary_key = 'permissionId';
     protected $soft_delete = TRUE;
     protected $soft_delete_key = 'isDeleted';
    /****************************************/
     
	/*
	* Rets the data as per given conditions and returns result
	* Also paging is considered here
	*/
	function select()
	{
            $this->arrLikeInputs = array('permissionName');
        $this->arrWhereInputs = array();
		
		if($this->input->post('permissionId') != '')
		{
			$this->wherearr['permissionId'] = $this->input->post('permissionId');
		}
                
		
		if($this->input->post('record_per_page') != '')
		{
			$records_per_page = $this->input->post('record_per_page');
		}
		else
		{
			$records_per_page = $this->config->item("ROWS_PER_PAGE");
		}
		if(!(isset($this->record_per_page) && $this->record_per_page != ''))
        {
            $this->record_per_page = $this->config->item("ROWS_PER_PAGE");
        }
		$this->arrWhere['P.isDeleted'] = 0;
                
		$this->arrLike = $this->get_like();
    	
        
		if($this->countQuery == 1)
		{
			$this->countSelectFields = 'COUNT(P.permissionId) as cnt';
			$this->db->from($this->tableName . ' ' . $this->alias);
        	$this->count =  $this->get_count();
        	$result['count'] = $this->count;
		}
		$result['resultarr'] = $this->get_result();
                //echo $this->db->last_query();die();
		$result['record_per_page'] = $records_per_page; /* chtn */
                
		return $result;
	}

	 
        
        /*
	* Select By RoleId
	* Selects permission of the particular Role
	*/
	function selectbyid($id = '')
	{
		$this->arrWhere['P.permissionId'] = $id;
                
        $this->selectFields = 'P.permissionId,P.permissionName,P.permissionFunction';


        $this->countQuery = 1;
        $result = $this->select();
        
        //$result['resultarr']=$result['resultarr'][0];
        
        return $result;
	}
        /*
	* Select By Permission
	* Selects permission
	*/
	function selectnotbyid($id = '')
	{
            $query = $this->db->select('permissionName')->where('permissionId != ',$id)->from("permission")->get()->result();
            return $query;
	}
	/*
	*Display all user data.
	*
	*/

	function get_list()
    {
    	//$langDataArr = $this->session->langData;
    	$this->selectFields = 'permissionId , permissionName,permissionFunction';

		$this->countQuery = 1;
		$this->sortBy='P.permissionId';
    	$result = $this->select();
    	return $result;
    }  

	/*
	* Insert / Update in users table
	* If @id is passed the Update else Add
	*/
	function insert_update($data, $tablename,$columnname='',$permissionId='')
	{
            if($permissionId == '')
            {
                if(!empty($data))
                {	
                    $this->db->insert($tablename, $data);
                    
                }

            }
            else
            {
                if(!empty($data))
                {	
                    $this->db->where($columnname, $permissionId);
                    $this->db->update($tablename, $data);
                
                }

            }
            return true;
	}

	/*
	* Delete in permission table
	*/
	function delete($id = '',$data)
	{
        $data['isDeleted'] = 1;
        $data['deletedDate'] = date("Y-m-d h:i:s");
		$sel_arr = $this->input->post('sel_arr');
		if(!empty($sel_arr))
		{
                    foreach($sel_arr as $row)
                    {

                            $this->db->where('permissionId', $row);
                            //$this->db->delete('permission'); 
                            $this->db->update('permission',$data);
                    }
		}
		else
		{
			//data['modifiedBy']= $this->session->userdata['userid'];
            //$data['modifiedBy']= date("Y-m-d h:i:s");

			$this->db->where('permissionId', $id);
                        //$this->db->delete('permission'); 
			$this->db->update('permission',$data);
		}
		return 1;
	}

	function check_permissionName($permissionName)
	{
            $this->selectFields = 'permissionName';

            $this->countQuery = 0;
            $this->arrWhere['P.permissionName'] = $permissionName;
            $arrresult = $this->select();
            $arrresult =$arrresult['resultarr'];

            if(isset($arrresult) && !empty($arrresult))
            {
                if($arrresult[0]->permissionName != '')
                {
                        return 1;
                }
                else
                {
                        return 0;

                }
            }
            else
            {
                    return 0;
            }
	}
        /*
         * this function insert permission into
         * RolePermision Table with default value i.e. ALL
         */
        function insert_rolePermission($permissionid)
        {
            $rolear = $this->getRolesIds();
            if(count($rolear)>0)
            {
                foreach ($rolear as $roles)
                {
                    $data = array(
                        'roleId'=>$roles,
                        'permissionId'=>$permissionid,
                        'assignedPerms'=>'0',
                        'createdDate'=>  get_current_date_time()
                    );
                    //$this->db->insert('roles_permission', $data);        
                }
            }
            return TRUE;
        }
        /*
         * this functiion get all roles
         */
        function getRolesIds()
        {
            $this->db->select("roleId");
            $this->db->from("roles");
            $query = $this->db->get();
            if($query->num_rows > 0)
            {
                $result = $query->result_array();
                $rolesar = array();
                foreach ($result as $roles)
                {
                    array_push($rolesar, $roles['roleId']);
                }
                return $rolesar;
            }
            else
            {
                return 0;
            }
        }
       
}

?>
