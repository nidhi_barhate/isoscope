<?php

class Referform_model extends MY_Model
{

    /****************************************/
     public $_table = 'referform';
     public $primary_key = 'referformId';
/*     protected $soft_delete = TRUE;
     protected $soft_delete_key = 'isDeleted';*/
    /****************************************/

    function get_referform_by_patient($patientId){
    	$this->db->select('*');
		$this->db->where('patientId',$patientId);
		$this->db->from('referform');
		$query = $this->db->get();
		return $query->result();
    }
}
?>
