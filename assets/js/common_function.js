function call_ajax(options) {
    jQuery.blockUI({
        message: blockUIElement,
        css: {
            top: ($(window).height() - 100) / 2 + 'px',
            left: ($(window).width() - 400) / 2 + 'px',
            width: '400px',
            display: 'block',
            background: 'white',
            border: '2px solid #555',

        }
    });
    var moduleUrl = options['moduleUrl'];
    var postData = options['postData'];
    postData[csrf_data.name] = csrf_data.value;
    //console.table(postData);
    jQuery.ajax({
        url: moduleUrl,
        type: "POST",
        data: postData,
        cache:false,
        success: function(data) {
            jQuery.unblockUI();
            jQuery('#divResponse').html(data);
        }
    });
}

function custom_call_ajax(options) {
  /*  jQuery.blockUI({
        message: blockUIElement,
        css: {
            top: ($(window).height() - 100) / 2 + 'px',
            left: ($(window).width() - 400) / 2 + 'px',
            width: '400px',
            display: 'block',
            background: 'white',
            border: '2px solid #555',

        }
    });*/
    var moduleUrl = options['moduleUrl'];
    var postData = options['postData'];
    var divResponse = postData['divResponse'];
    postData[csrf_data.name] = csrf_data.value;
    console.log(postData);
    //console.log(moduleUrl,postData,divResponse);
    jQuery.ajax({
        url: moduleUrl,
        type: "POST",
        data: postData,
        cache:false,
        //async: false,
        success: function(data) {
            //console.log(data);
          //  jQuery.unblockUI();
            jQuery('#' + divResponse).html(data);
            var flag = postData['custom_function_flag'];
            if(flag === 1){
                do_custom_function()
            }
            /*if(flag === 2){
                do_custom_function1()
            }*/

        }
    });
}


$('#divResponse').on('click','.deleteConfirm', function(){
    var moduleUrl = $(this).data('moduleurl');

    var r = confirm("Are You Sure Want to delete?");
    if (r == true) {
            var postData = {}  ;
            /*jQuery.blockUI({
                message: blockUIElement,
                css: {
                    top: ($(window).height() - 100) / 2 + 'px',
                    left: ($(window).width() - 400) / 2 + 'px',
                    width: '400px',
                    display: 'block',
                    background: 'white',
                    border: '2px solid #555',

                }
            });*/
            postData[csrf_data.name] = csrf_data.value;
            //console.table(postData);
            jQuery.ajax({
                url: moduleUrl,
                type: "POST",
                data: postData,
                cache:false,
                success: function(data) {
                    //jQuery.unblockUI();
                    location.reload(true);
                }
            });
    } else {
        return false;
    }
})